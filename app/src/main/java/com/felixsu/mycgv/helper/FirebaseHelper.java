package com.felixsu.mycgv.helper;

import android.os.Bundle;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created on 12/25/16.
 *
 * @author felixsoewito
 */

public class FirebaseHelper {

    private static final String EVENT_SELECT_PREFERENCE = "preference";
    private static final String EVENT_MOVIE_ERROR = "cgvError";

    public static void logEventOpenMovie(FirebaseAnalytics instance, Movie movie) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, movie.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, movie.getMovieName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "movie");
        instance.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logEventOpenCinema(FirebaseAnalytics instance, Cinema cinema) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, cinema.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, cinema.getCinemaName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "cinema");
        instance.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logEventSelectCity(FirebaseAnalytics instance, String city) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "city");
        bundle.putString(FirebaseAnalytics.Param.VALUE, city);
        instance.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logEventMovieError(FirebaseAnalytics instance, String id) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "movie");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        instance.logEvent(EVENT_MOVIE_ERROR, bundle);
    }

    public static void logEventOpenTrailer(FirebaseAnalytics instance, Movie movie) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, movie.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, movie.getMovieName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "trailer");
        instance.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }


}
