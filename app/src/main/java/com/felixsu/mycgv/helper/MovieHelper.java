package com.felixsu.mycgv.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created on 08/01/2017.
 *
 * @author felixsu
 */

public class MovieHelper {

    public static void openTrailer(Context context, String youtubeUrl){
        context.startActivity(
                new Intent(Intent.ACTION_VIEW,
                        Uri.parse(youtubeUrl)));
    }
}
