package com.felixsu.mycgv.helper;

import android.os.Build;
import android.text.TextUtils;

import com.felixsu.mycgv.BuildConfig;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util {

    private static final DecimalFormat MONEY_FORMAT = new DecimalFormat("###,###");
    private static final SimpleDateFormat SCHEDULE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        //String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                //phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            //phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public static String getAndroidVersion() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    public static String formatAsNumber(long number){
        return MONEY_FORMAT.format(number);
    }

    public static String getScheduleDate() {
        return SCHEDULE_DATE_FORMAT.format(Calendar.getInstance().getTime());
    }
}