package com.felixsu.mycgv.helper;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.common.model.cinema.Schedule;

import java.net.URLEncoder;
import java.util.List;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

public class CinemaHelper {

    @NonNull
    public static String[] findAddress(List<CinemaDetail> details) {
        String result = "-#-";
        for (CinemaDetail detail : details) {
            if (detail.getType().equals(CinemaDetail.DetailType.ADDRESS)) {
                result = detail.getValue();
            }
        }

        return result.split("#");
    }

    @NonNull
    public static String findFeatures(List<CinemaDetail> details) {
        StringBuilder sb = new StringBuilder();
        for (CinemaDetail detail : details) {
            if (detail.getType().equals(CinemaDetail.DetailType.FEATURE)) {
                sb.append(detail.getValue()).append(", ");
            }
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length()-1);
        }

        return sb.toString();
    }

    public static String findPhoneNumbers(List<CinemaDetail> details) {
        String result = "";

        for (CinemaDetail detail : details) {
            if (detail.getType().equals(CinemaDetail.DetailType.PHONE)) {
                if (!detail.getValue().equals("-")) {
                    result = result.isEmpty() ? detail.getValue() : result.concat(", " + detail.getValue());
                }
            }
        }

        return result.isEmpty() ? "-" : result;
    }

    public static boolean isValidCinema(Cinema cinema) {
        boolean result = true;

        if ((cinema.getCinemaDetails() == null) || cinema.getCinemaDetails().isEmpty()) {
            result = false;
        }

        if ((cinema.getCinemaPrices() == null) || cinema.getCinemaPrices().isEmpty()) {
            result = false;
        }

        return result;
    }

    public static String generateReservation(Schedule schedule, String time) {
        //String example = "https://www.cgv.id/en/schedule/view_seat_layout/692316?showdate=2018-06-24&cinema=002"

        final String QUERY_CINEMA = "cinema";
        final String QUERY_SHOW_DATE = "showdate";

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.cgv.id")
                .appendPath("en")
                .appendPath("schedule")
                .appendPath("view_seat_layout")
                .appendPath(schedule.getMovieId())
                .appendQueryParameter(QUERY_CINEMA, schedule.getCinemaId())
                .appendQueryParameter(QUERY_SHOW_DATE, Util.getScheduleDate())
        ;

        return builder.build().toString();
    }
}
