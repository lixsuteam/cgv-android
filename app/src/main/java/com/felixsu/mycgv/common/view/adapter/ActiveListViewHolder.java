package com.felixsu.mycgv.common.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created on 12/14/16.
 *
 * @author felixsoewito
 */

public abstract class ActiveListViewHolder<T>
        extends ListViewHolder<T>
        implements View.OnClickListener, View.OnLongClickListener {

    private final ListAdapter.ItemClickListener mItemClickListener;
    private final ListAdapter.ItemLongClickListener mItemLongClickListener;

    public ActiveListViewHolder(View itemView,
                                ListAdapter.ItemClickListener itemClickListener,
                                ListAdapter.ItemLongClickListener itemLongClickListener) {
        super(itemView);
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);

        mItemClickListener = itemClickListener;
        mItemLongClickListener = itemLongClickListener;
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener != null) {
            mItemClickListener.onClick(v, getLayoutPosition());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mItemLongClickListener != null) {
            mItemLongClickListener.onLongClick(v, getLayoutPosition());
            return true;
        }
        return false;
    }

    public abstract void bindView(T item);
}