package com.felixsu.mycgv.common.dagger.component;

import android.content.SharedPreferences;

import com.felixsu.mycgv.common.dagger.module.AppModule;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created on 12/15/16.
 *
 * @author felixsoewito
 *
 */

@Singleton
@Component(modules = AppModule.class)
public interface CgvAppComponent {

    SharedPreferences sharedPreferences();

    CgvService cgvService();

    EventBus eventBus();

    SharedData sharedData();
}
