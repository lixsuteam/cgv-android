package com.felixsu.mycgv.common.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * Created on 12/14/16.
 *
 * @author felixsoewito
 */

public abstract class ListAdapter<T, V extends ListViewHolder<T>>
        extends RecyclerView.Adapter<V> {

    protected ItemClickListener mItemClickListener;
    protected ItemLongClickListener mItemLongClickListener;
    protected List<T> mItems;

    public ListAdapter(@NonNull List<T> items) {
        mItems = items;
    }

    @Override
    public void onBindViewHolder(V holder, int position) {
        holder.bindView(getItem(position));
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public T getItem(int position) {
        if (mItems == null){
            return null;
        } else if (position < mItems.size()){
            return mItems.get(position);
        } else {
            return null;
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener) {
        mItemLongClickListener = itemLongClickListener;
    }

    public interface ItemClickListener {
        void onClick(View v, int i);
    }

    public interface ItemLongClickListener {
        void onLongClick(View v, int i);
    }

    public List<T> getItems() {
        return mItems;
    }

    public void setItems(List<T> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}
