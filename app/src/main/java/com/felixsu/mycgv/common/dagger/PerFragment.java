package com.felixsu.mycgv.common.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 *
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}
