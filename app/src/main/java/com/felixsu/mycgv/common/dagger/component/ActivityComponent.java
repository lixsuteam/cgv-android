package com.felixsu.mycgv.common.dagger.component;

import com.felixsu.mycgv.common.dagger.PerActivity;
import com.felixsu.mycgv.common.dagger.module.ActivityModule;
import com.felixsu.mycgv.common.dagger.module.AppModule;
import com.felixsu.mycgv.view.main.MainActivity;
import com.felixsu.mycgv.view.splash.SplashActivity;
import com.felixsu.mycgv.view.wizard.WizardActivity;

import dagger.Component;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 *
 */

@PerActivity
@Component(
        dependencies = CgvAppComponent.class,
        modules = ActivityModule.class
)
public interface ActivityComponent {

    void inject(SplashActivity activity);
    void inject(WizardActivity activity);
    void inject(MainActivity activity);

}
