package com.felixsu.mycgv.common.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created on 1/5/17.
 *
 * @author felixsoewito
 */
public abstract class ListViewHolder<T> extends RecyclerView.ViewHolder {

    public ListViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindView(T item);
}
