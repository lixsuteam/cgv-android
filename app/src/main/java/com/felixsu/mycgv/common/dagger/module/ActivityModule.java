package com.felixsu.mycgv.common.dagger.module;

import android.app.Activity;

import com.felixsu.mycgv.common.dagger.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 *
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @PerActivity
    @Provides
    Activity provideActivity() {
        return mActivity;
    }
}
