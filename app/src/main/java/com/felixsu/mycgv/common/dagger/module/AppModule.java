package com.felixsu.mycgv.common.dagger.module;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.felixsu.mycgv.MyCgvApp;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 *
 */

@Module
public class AppModule {

    private MyCgvApp mApp;

    public AppModule(MyCgvApp app) {
        mApp = app;
    }

    @Provides
    @Singleton
    public MyCgvApp providesApplication() {
        return mApp;
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreference(MyCgvApp application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    public EventBus providesEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    public CgvService providesCgvService() {
        return CgvService.Creator.newCgvService();
    }

    @Provides
    @Singleton
    public SharedData providesSharedData() {
        return new SharedData();
    }
}
