package com.felixsu.mycgv.common.view.mvp;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.io.Serializable;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public interface ViewStateView<M extends Serializable> extends MvpView {

    M getData();

}
