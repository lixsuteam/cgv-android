package com.felixsu.mycgv.common.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.felixsu.mycgv.MyCgvApp;

import butterknife.ButterKnife;
import icepick.Icepick;
import io.fabric.sdk.android.Fabric;

/**
 * Base class for Activities which already setup butterknife and icepick
 * <p>
 * Created on  on 12/10/16.
 *
 * @author felixsoewito
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    protected void injectDependencies() {

    }

    public MyCgvApp getBaseApplication() {
        return (MyCgvApp) getApplication();
    }
}