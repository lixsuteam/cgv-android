package com.felixsu.mycgv.provider.data.local;

import android.content.SharedPreferences;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 */


public class PersistanceHelper {

    public static final String KEY_CITY_SUBSCRIPTION = "city_subscription";

    public static void putCity(SharedPreferences.Editor editor, String city) {
        editor.putString(KEY_CITY_SUBSCRIPTION, city);
        editor.commit();
    }

    public static String getCity(SharedPreferences preferences) {
        return preferences.getString(KEY_CITY_SUBSCRIPTION, null);
    }
}
