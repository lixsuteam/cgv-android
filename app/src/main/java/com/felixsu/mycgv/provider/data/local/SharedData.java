package com.felixsu.mycgv.provider.data.local;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * contains all shared variable across application
 * Created on 12/15/16.
 *
 * @author felixsoewito
 *
 */
public class SharedData {

    private static final Map<String, Object> mSharedData = new HashMap<>();

    private static final String KEY_CITY = "key-city";

    private static final String KEY_CONTENT_CINEMA = "key-content-cinema";

    private static final String KEY_CONTENT_NOW_PLAYING = "key-content-now-playing";
    private static final String KEY_CONTENT_COMING_SOON = "key-content-coming-soon";
    private static final String KEY_SYNCHRONIZED_NOW_PLAYING = "key-synchronized-now-playing";

    private static final String KEY_CONTENT_CINEMA_SCHEDULE = "key-content-cinema-schedule";
    private static final String KEY_CONTENT_MOVIE_SCHEDULE = "key-content-movie-schedule";
    private static final String KEY_FETCHED_CINEMA_SCHEDULE = "key-fetched-cinema-schedule";
    private static final String KEY_FETCHED_MOVIE_SCHEDULE = "key-fetched-movie-schedule";
    private static final String KEY_SYNCHRONIZED_CINEMA_SCHEDULE = "key-synchronized-cinema-schedule";
    private static final String KEY_SYNCHRONIZED_MOVIE_SCHEDULE = "key-synchronized-movie-schedule";

    //general
    private String getString(String key){
        return (String) mSharedData.get(key);
    }

    private Long getLong(String key) {
        return (Long) mSharedData.get(key);
    }

    //city
    public void putCity(String city) {
        mSharedData.put(KEY_CITY, city);
    }

    public String getCity() {
        return getString(KEY_CITY);
    }

    //cinemas
    public void putCinemaContent(List<Cinema> cinemas) {
        mSharedData.put(KEY_CONTENT_CINEMA, cinemas);
    }

    @SuppressWarnings("unchecked")
    public List<Cinema> getCinemaContent() {
        return (List<Cinema>) mSharedData.get(KEY_CONTENT_CINEMA);
    }

    //movies
    public void putNowPlayingContent(List<Movie> movies) {
        mSharedData.put(KEY_CONTENT_NOW_PLAYING, movies);
    }

    public void putComingSoonContent(List<Movie> movies) {
        mSharedData.put(KEY_CONTENT_COMING_SOON, movies);
    }

    public void putNowPlayingSynchronized(Long time) {
        mSharedData.put(KEY_SYNCHRONIZED_NOW_PLAYING, time);
    }

    @SuppressWarnings("unchecked")
    public List<Movie> getNowPlayingContent() {
        return (List<Movie>) mSharedData.get(KEY_CONTENT_NOW_PLAYING);
    }

    @SuppressWarnings("unchecked")
    public List<Movie> getComingSoonContent() {
        return (List<Movie>) mSharedData.get(KEY_CONTENT_COMING_SOON);
    }

    public Long getNowPlayingSynchronized() {
        return getLong(KEY_SYNCHRONIZED_NOW_PLAYING);
    }

    //schedule
    public void putCinemaScheduleContent(ArrayList<ArrayList<Schedule>> content) {
        mSharedData.put(KEY_CONTENT_CINEMA_SCHEDULE, content);
    }

    public void putMovieScheduleContent(ArrayList<ArrayList<Schedule>> content) {
        mSharedData.put(KEY_CONTENT_MOVIE_SCHEDULE, content);
    }

    public void putCinemaScheduleFetched(String id) {
        mSharedData.put(KEY_FETCHED_CINEMA_SCHEDULE, id);
    }

    public void putMovieScheduleFetched(String id) {
        mSharedData.put(KEY_FETCHED_MOVIE_SCHEDULE, id);
    }

    public String getCinemaScheduleFetched() {
        return (String) mSharedData.get(KEY_FETCHED_CINEMA_SCHEDULE);
    }

    public String getMovieScheduleFetched() {
        return (String) mSharedData.get(KEY_FETCHED_MOVIE_SCHEDULE);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<ArrayList<Schedule>> getCinemaScheduleContent() {
        return (ArrayList<ArrayList<Schedule>>) mSharedData.get(KEY_CONTENT_CINEMA_SCHEDULE);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<ArrayList<Schedule>> getMovieScheduleContent() {
        return (ArrayList<ArrayList<Schedule>>) mSharedData.get(KEY_CONTENT_MOVIE_SCHEDULE);
    }

    public void putCinemaScheduleSynchronized(Long time) {
        mSharedData.put(KEY_SYNCHRONIZED_CINEMA_SCHEDULE, time);
    }

    public void putMovieScheduleSynchronized(Long time) {
        mSharedData.put(KEY_SYNCHRONIZED_MOVIE_SCHEDULE, time);
    }

    public Long getCinemaScheduleSynchronized() {
        return (Long) mSharedData.get(KEY_SYNCHRONIZED_CINEMA_SCHEDULE);
    }

    public Long getMovieScheduleSynchronized() {
        return (Long) mSharedData.get(KEY_SYNCHRONIZED_MOVIE_SCHEDULE);
    }
}
