package com.felixsu.mycgv.provider.data.remote;

import com.felixsu.common.helper.JsonHelper;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by felixsoewito on 12/18/16.
 */

public interface CgvService {

    String BASE_URL = BuildConfig.API_BASE_URL;

    @GET("movie")
    Observable<RestResponse<Movie>> getMovies(@Query("status") String status,
                                              @Query("genre") String genre,
                                              @Query("city") String city,
                                              @Query("owner") String owner);

    @GET("movie/{id}")
    Observable<RestResponse<Movie>> getMovie(@Path("id") String id);

    @GET("schedule")
    Observable<RestResponse<Schedule>> getSchedules(@Query("city") String city, @Query("movie") String movie);

    @GET("schedule")
    Observable<RestResponse<Schedule>> getSchedules(@Query("cinema") String cinema);

    @GET("cinema")
    Observable<RestResponse<Cinema>> getCinemas(@Query("city") String city,
                                                @Query("owner") String owner);

    class Creator {
        public static CgvService newCgvService() {
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .registerTypeAdapter(Date.class, new JsonDeserializer() {
                        @Override
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
            OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
            okHttpClient.interceptors().add(logging);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(CgvService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient.build())
                    .build();

            return retrofit.create(CgvService.class);
        }
    }
}
