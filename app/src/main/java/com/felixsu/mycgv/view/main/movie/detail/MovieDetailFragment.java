package com.felixsu.mycgv.view.main.movie.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.BuildConfig;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.fragment.BaseViewStateFragment;
import com.felixsu.mycgv.helper.FirebaseHelper;
import com.felixsu.mycgv.helper.MovieHelper;
import com.felixsu.mycgv.view.main.movie.detail.adapter.ScheduleParentAdapter;
import com.felixsu.mycgv.view.main.movie.detail.dagger.DaggerMovieDetailComponent;
import com.felixsu.mycgv.view.main.movie.detail.dagger.MovieDetailComponent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;

/**
 * Created on 12/20/16.
 *
 * @author felixsoewito
 */

public class MovieDetailFragment extends BaseViewStateFragment<MovieDetailView, MovieDetailPresenter, MovieDetailViewState>
        implements MovieDetailView {

    public static final String TAG = MovieDetailFragment.class.getName();

    //VIEW
    //static
    @BindView(R.id.container_primary) View mPrimaryContainer;
    @BindView(R.id.label_title) TextView mTitleLabel;
    @BindView(R.id.image_cover) ImageView mCoverImage;
    @BindView(R.id.label_duration) TextView mDurationLabel;
    @BindView(R.id.label_rating) TextView mRatingLabel;
    @BindView(R.id.label_directors) TextView mDirectorsLabel;
    @BindView(R.id.label_genre) TextView mGenreLabel;
    @BindView(R.id.label_language) TextView mLanguageLabel;
    @BindView(R.id.label_movie_actors) TextView mActorsLabel;
    @BindView(R.id.label_description_value) TextView mDescriptionLabel;

    //loading
    @BindView(R.id.container_loading) View mLoadingContainer;
    //content
    @BindView(R.id.container_content) View mContentContainer;
    @BindView(R.id.container_schedule)
    RecyclerView mScheduleContainer;
    //error
    @BindView(R.id.container_error) View mErrorContainer;

    //DATA
    MovieDetailComponent mComponent;
    String mErrorMessage;
    @State @Arg String mId;
    @State @Arg String mTitle;
    @State @Arg String mCoverUrl;
    @State @Arg String mDuration;
    @State @Arg String mRating;
    @State @Arg String mDirectors;
    @State @Arg String mGenre;
    @State @Arg String mLanguage;
    @State @Arg String mActors;
    @State @Arg String mDescription;
    @State @Arg String mTrailerUrl;

    //ADAPTER
    private ScheduleParentAdapter mAdapter;

    //LISTENER
    private View.OnClickListener mMovieClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMovieClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTrailerUrl != null) {
                    MovieHelper.openTrailer(getContext(), mTrailerUrl);
                    Toast.makeText(getContext(), R.string.text_opening_trailer, Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseHelper.logEventMovieError(FirebaseAnalytics.getInstance(getContext()), mId);
                    Toast.makeText(getContext(), R.string.text_trailer_not_available, Toast.LENGTH_SHORT).show();
                }
            }
        };

        mPrimaryContainer.setOnClickListener(mMovieClickListener);
        mTitleLabel.setText(mTitle);
        mDurationLabel.setText(mDuration);
        mRatingLabel.setText(mRating);
        mDirectorsLabel.setText(mDirectors);
        mGenreLabel.setText(mGenre);
        mLanguageLabel.setText(mLanguage);
        mActorsLabel.setText(mActors);
        mDescriptionLabel.setText(mDescription);
        Picasso.with(getContext()).load(BuildConfig.CGV_BASE_URL + mCoverUrl)
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(mCoverImage);

        mAdapter = new ScheduleParentAdapter(getContext(), getContent());

        mScheduleContainer.setAdapter(mAdapter);
        mScheduleContainer.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mScheduleContainer.setNestedScrollingEnabled(false);
        mScheduleContainer.setHasFixedSize(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMovieClickListener = null;
        mAdapter = null;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_movie_detail;
    }

    @NonNull
    @Override
    public MovieDetailViewState createViewState() {
        return new MovieDetailViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.load(mId);
    }

    @NonNull
    @Override
    public MovieDetailPresenter createPresenter() {
        return mComponent.presenter();
    }

    @Override
    public void showContent() {
        MovieDetailViewState vs = (MovieDetailViewState) getViewState();
        vs.setContent(getContent());

        mLoadingContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        MovieDetailViewState vs = (MovieDetailViewState) getViewState();
        vs.setError();

        mContentContainer.setVisibility(View.GONE);
        mLoadingContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        MovieDetailViewState vs = (MovieDetailViewState) getViewState();
        vs.setLoading();

        mContentContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.GONE);
        mLoadingContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void setContent(ArrayList<ArrayList<Schedule>> schedules) {
        mAdapter.setItems(schedules);
    }

    @Override
    public void setError(String message) {
        mErrorMessage = message;
    }

    @Override
    public ArrayList<ArrayList<Schedule>> getContent() {
        return mAdapter != null ? (ArrayList<ArrayList<Schedule>>) mAdapter.getItems() : null;
    }

    @Override
    public String getError() {
        return mErrorMessage;
    }

    @Override
    protected void injectDependencies() {
        mComponent = DaggerMovieDetailComponent.builder()
                .cgvAppComponent(getBaseActivity().getBaseApplication().getComponent())
                .build();
        mComponent.inject(this);
    }

    @OnClick(R.id.button_schedule_refresh)
    public void refreshSchedule() {
        presenter.load(mId);
    }
}
