package com.felixsu.mycgv.view.main.movie.detail;

import android.util.Log;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 12/20/16.
 *
 * @author felixsoewito
 */

public class MovieDetailPresenter extends MvpBasePresenter<MovieDetailView> {

    private static final String TAG = MovieDetailPresenter.class.getName();

    public static final long SCHEDULE_VALIDITY = 10*60*1000; //10 minutes

    private Subscriber<RestResponse<Schedule>> mCinemaSubscriber;
    private CgvService mCgvService;
    private SharedData mSharedData;
    private String mMovieId;

    @Inject
    public MovieDetailPresenter(CgvService cgvService, SharedData sharedData) {
        mCgvService = cgvService;
        mSharedData = sharedData;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            cancelSubscription();
        }
    }

    public void load(String movieId) {
        mMovieId = movieId;
        subscribeSchedule(mCgvService.getSchedules(mSharedData.getCity(), movieId));
    }

    private void subscribeSchedule(Observable<RestResponse<Schedule>> observable){
        long currentTime = new Date().getTime();
        long lastStore = mSharedData.getMovieScheduleSynchronized() == null ? 0L : mSharedData.getMovieScheduleSynchronized();
        String fetchedLastTime = mSharedData.getMovieScheduleFetched() == null ? "" : mSharedData.getMovieScheduleFetched();

        boolean isScheduleValid = (currentTime - lastStore) < SCHEDULE_VALIDITY;
        boolean isLoaded = mSharedData.getMovieScheduleContent() != null;
        boolean isScheduleEquals = fetchedLastTime.equals(mMovieId);

        if (isLoaded && isScheduleValid && isScheduleEquals) {
            if (isViewAttached()) {
                getView().setContent(mSharedData.getMovieScheduleContent());
                getView().showContent();
            }
            return;
        }

        if (isViewAttached()) {
            getView().showLoading();
        }

        cancelSubscription();

        mCinemaSubscriber = new Subscriber<RestResponse<Schedule>>() {
            @Override
            public void onCompleted() {
                if (isViewAttached()) {
                    getView().showContent();
                }
                cancelSubscription();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                if (isViewAttached()) {
                    getView().showError();
                }
                cancelSubscription();
            }

            @Override
            public void onNext(RestResponse<Schedule> response) {
                List<Schedule> schedules = response.getResponses();
                Map<String, ArrayList<Schedule>> mapper = new HashMap<>();

                for (Schedule s : schedules) {
                    String cinema = s.getCinemaId();

                    if (mapper.containsKey(cinema)) {
                        mapper.get(cinema).add(s);
                    } else {
                        ArrayList<Schedule> l = new ArrayList<>();
                        l.add(s);
                        mapper.put(cinema, l);
                    }
                }

                ArrayList<ArrayList<Schedule>> result = new ArrayList<>();
                for (Map.Entry<String, ArrayList<Schedule>> entry : mapper.entrySet()) {
                    result.add(entry.getValue());
                }

                mSharedData.putMovieScheduleSynchronized(new Date().getTime());
                mSharedData.putMovieScheduleContent(result);
                mSharedData.putMovieScheduleFetched(mMovieId);

                if (isViewAttached()) {
                    getView().setContent(result);
                }
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mCinemaSubscriber);
    }

    private void cancelSubscription() {
        if (mCinemaSubscriber != null && !mCinemaSubscriber.isUnsubscribed()) {
            mCinemaSubscriber.unsubscribe();
        }
        mCinemaSubscriber = null;
    }
}
