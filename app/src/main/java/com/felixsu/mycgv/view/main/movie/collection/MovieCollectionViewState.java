package com.felixsu.mycgv.view.main.movie.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.felixsu.common.model.cinema.Movie;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public class MovieCollectionViewState implements RestorableViewState<MovieCollectionView> {

    public static final String KEY_STATE = "key-mState";
    public static final String KEY_CONTENT_NOW_PLAYING = "key-content-now-playing";
    public static final String KEY_CONTENT_COMING_SOON = "key-content-coming-soon";
    public static final String KEY_ERROR_NOW_PLAYING = "key-error-now-playing";
    public static final String KEY_ERROR_COMING_SOON = "key-error-coming-soon";

    private final int SHOW_NOW_PLAYING_CONTENT = 0x01;
    private final int SHOW_NOW_PLAYING_LOADING = 0x02;
    private final int SHOW_NOW_PLAYING_ERROR = 0x04;

    private final int SHOW_COMING_SOON_CONTENT = 0x10;
    private final int SHOW_COMING_SOON_LOADING = 0x20;
    private final int SHOW_COMING_SOON_ERROR = 0x40;

    private int mState = SHOW_NOW_PLAYING_LOADING + SHOW_COMING_SOON_LOADING;

    private ArrayList<Movie> mNowPlaying;
    private ArrayList<Movie> mComingSoon;
    private String mErrorNowPlaying;
    private String mErrorComingSoon;

    void setNowPlayingContent(List<Movie> movies) {
        mNowPlaying = (ArrayList<Movie>) movies;
        mState &= 0xF0;
        mState |= SHOW_NOW_PLAYING_CONTENT;
    }

    void setComingSoonContent(List<Movie> movies) {
        mComingSoon = (ArrayList<Movie>) movies;
        mState &= 0x0F;
        mState |= SHOW_COMING_SOON_CONTENT;
    }

    void setNowPlayingLoading() {
        mState &= 0xF0;
        mState |= SHOW_NOW_PLAYING_LOADING;
    }

    void setComingSoonLoading() {
        mState &= 0x0F;
        mState |= SHOW_COMING_SOON_LOADING;
    }

    void setNowPlayingError(String message) {
        mErrorNowPlaying = message;
        mState &= 0xF0;
        mState |= SHOW_NOW_PLAYING_ERROR;
    }

    void setComingSoonError(String message) {
        mErrorComingSoon = message;
        mState &= 0x0F;
        mState |= SHOW_COMING_SOON_ERROR;
    }

    @Override
    public void apply(MovieCollectionView view, boolean retained) {
        switch (mState) {
            case 0x11:
                view.setComingSoon(mComingSoon);
                view.showContentComingSoon();

                view.setNowPlaying(mNowPlaying);
                view.showContentNowPlaying();
                break;
            case 0x12:
                view.setComingSoon(mComingSoon);
                view.showContentComingSoon();

                view.showLoadingNowPlaying();
                break;
            case 0x14:
                view.setComingSoon(mComingSoon);
                view.showContentComingSoon();

                view.setErrorNowPlaying(mErrorNowPlaying);
                view.showErrorNowPlaying();
                break;

            case 0x21:
                view.showLoadingComingSoon();

                view.setNowPlaying(mNowPlaying);
                view.showContentNowPlaying();
                break;
            case 0x22:
                view.showLoadingComingSoon();

                view.showLoadingNowPlaying();
                break;
            case 0x24:
                view.showLoadingComingSoon();

                view.setErrorNowPlaying(mErrorNowPlaying);
                view.showErrorNowPlaying();
                break;

            case 0x41:
                view.setErrorComingSoon(mErrorComingSoon);
                view.showErrorComingSoon();

                view.setNowPlaying(mNowPlaying);
                view.showContentNowPlaying();
                break;
            case 0x42:
                view.setErrorComingSoon(mErrorComingSoon);
                view.showErrorComingSoon();

                view.showLoadingNowPlaying();
                break;
            case 0x44:
                view.setErrorComingSoon(mErrorComingSoon);
                view.showErrorComingSoon();

                view.setErrorNowPlaying(mErrorNowPlaying);
                view.showErrorNowPlaying();
                break;
            default:
                view.setErrorComingSoon(mErrorComingSoon);
                view.showErrorComingSoon();

                view.setErrorNowPlaying(mErrorNowPlaying);
                view.showErrorNowPlaying();
                break;
        }
    }

    @Override
    public void saveInstanceState(@NonNull Bundle out) {
        out.putInt(KEY_STATE, mState);
        out.putSerializable(KEY_CONTENT_NOW_PLAYING, mNowPlaying);
        out.putSerializable(KEY_CONTENT_COMING_SOON, mComingSoon);
        out.putSerializable(KEY_ERROR_NOW_PLAYING, mErrorNowPlaying);
        out.putSerializable(KEY_ERROR_COMING_SOON, mErrorComingSoon);
    }

    @SuppressWarnings("unchecked")
    @Override
    public RestorableViewState<MovieCollectionView> restoreInstanceState(Bundle in) {
        if (in == null) {
            return null;
        }

        mState = in.getInt(KEY_STATE);
        mNowPlaying = (ArrayList<Movie>) in.getSerializable(KEY_CONTENT_NOW_PLAYING);
        mComingSoon = (ArrayList<Movie>) in.getSerializable(KEY_CONTENT_COMING_SOON);
        mErrorNowPlaying = (String) in.getSerializable(KEY_ERROR_NOW_PLAYING);
        mErrorComingSoon = (String) in.getSerializable(KEY_ERROR_COMING_SOON);
        return this;
    }
}
