package com.felixsu.mycgv.view.main.movie.collection.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;

import java.util.List;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public class NowPlayingAdapter extends ListAdapter<Movie, NowPLayingViewHolder> {

    final private Context mContext;

    public NowPlayingAdapter(Context context, @NonNull List<Movie> items) {
        super(items);
        mContext = context;
    }

    @Override
    public NowPLayingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_now_playing, parent, false);
        return new NowPLayingViewHolder(view, mItemClickListener, mItemLongClickListener, mContext);
    }
}
