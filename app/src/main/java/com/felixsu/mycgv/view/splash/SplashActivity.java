package com.felixsu.mycgv.view.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.dagger.component.ActivityComponent;
import com.felixsu.mycgv.common.dagger.component.DaggerActivityComponent;
import com.felixsu.mycgv.provider.data.local.PersistanceHelper;
import com.felixsu.mycgv.common.view.activity.BaseActivity;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.view.main.MainActivity;
import com.felixsu.mycgv.view.wizard.WizardActivity;

import javax.inject.Inject;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 *
 */
public class SplashActivity extends BaseActivity {

    private static final int SPLASH_TIME_OUT = 1000;

    Handler mTimerHandler;

    @Inject
    SharedPreferences mSharedPreferences;

    @Inject
    SharedData mSharedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onBackPressed() {
        //no op
    }

    @Override
    protected void onStart() {
        super.onStart();
        final String PERSISTED_CITY = PersistanceHelper.getCity(mSharedPreferences);

        mTimerHandler = new Handler();
        mTimerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;

                if (PERSISTED_CITY != null) {
                    mSharedData.putCity(PERSISTED_CITY);
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, WizardActivity.class);
                }

                intent
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onStop() {
        //stop callback on app stop
        mTimerHandler.removeCallbacks(null);
        super.onStop();
    }

    @Override
    protected void injectDependencies() {
        ActivityComponent component = DaggerActivityComponent.builder()
                .cgvAppComponent(getBaseApplication().getComponent())
                .build();
        component.inject(this);
    }
}
