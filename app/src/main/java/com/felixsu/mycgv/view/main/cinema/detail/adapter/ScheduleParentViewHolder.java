package com.felixsu.mycgv.view.main.cinema.detail.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.BuildConfig;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.felixsu.mycgv.view.main.movie.detail.adapter.ScheduleFirstChildAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

public class ScheduleParentViewHolder extends ActiveListViewHolder<ArrayList<Schedule>> {

    private static final String TAG =  ScheduleParentViewHolder.class.getName();

    private Subscriber<RestResponse<Movie>> mMovieSubscriber;
    //VIEW
    //static
    private RecyclerView mListContainer;
    private final Context mContext;
    private final CgvService mCgvService;

    //LOADING
    private View mLoadingContainer1;
    private View mLoadingContainer2;
    //CONTENT
    private TextView mMovieNameLabel;
    private ImageView mMovieCoverImage;
    //ERROR
    private View mErrorContainer1;
    private View mErrorContainer2;
    private Button mRefreshButton;

    //DATA
    String mId;
    Schedule mSchedule;

    public ScheduleParentViewHolder(View itemView,
                                    ListAdapter.ItemClickListener itemClickListener,
                                    ListAdapter.ItemLongClickListener itemLongClickListener,
                                    Context context,
                                    CgvService cgvService) {
        super(itemView, itemClickListener, itemLongClickListener);
        mContext = context;
        mCgvService = cgvService;

        mLoadingContainer1 = itemView.findViewById(R.id.container_loading);
        mLoadingContainer2 = itemView.findViewById(R.id.container_loading_2);

        mErrorContainer1 = itemView.findViewById(R.id.container_error);
        mErrorContainer2 = itemView.findViewById(R.id.container_error_2);
        mRefreshButton = (Button) itemView.findViewById(R.id.button_refresh);

        mMovieNameLabel = (TextView) itemView.findViewById(R.id.label_movie_name);
        mMovieCoverImage = (ImageView) itemView.findViewById(R.id.image_movie_cover);

        mListContainer = (RecyclerView) itemView.findViewById(R.id.container_schedule_child);
        mListContainer.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mListContainer.setHasFixedSize(true);
        mListContainer.setNestedScrollingEnabled(false);
    }

    @Override
    public void bindView(ArrayList<Schedule> item) {
        mSchedule = item.size() > 0 ? item.get(0) : null;
        mId = item.get(0).getMovieId();

        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load();
            }
        });

        ScheduleFirstChildAdapter adapter = new ScheduleFirstChildAdapter(mContext, item);
        mListContainer.setAdapter(adapter);
    }

    public void load () {
        subscribe(mCgvService.getMovie(mId));
    }

    public void unsubscribe() {
        if (mMovieSubscriber != null && !mMovieSubscriber.isUnsubscribed()) {
            mMovieSubscriber.unsubscribe();
        }
        mMovieSubscriber = null;
    }

    private void showLoading() {

        mErrorContainer1.setVisibility(View.GONE);
        mErrorContainer2.setVisibility(View.GONE);

        mMovieNameLabel.setVisibility(View.GONE);
        mMovieCoverImage.setVisibility(View.GONE);

        mLoadingContainer1.setVisibility(View.VISIBLE);
        mLoadingContainer2.setVisibility(View.VISIBLE);
    }

    private void showContent(String coverUrl, String movieName) {
        mMovieNameLabel.setText(movieName);
        Picasso.with(mContext).load(BuildConfig.CGV_BASE_URL + coverUrl)
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(mMovieCoverImage);

        mLoadingContainer1.setVisibility(View.GONE);
        mLoadingContainer2.setVisibility(View.GONE);

        mErrorContainer1.setVisibility(View.GONE);
        mErrorContainer2.setVisibility(View.GONE);

        mMovieNameLabel.setVisibility(View.VISIBLE);
        mMovieCoverImage.setVisibility(View.VISIBLE);
    }

    private void showError() {

        mMovieNameLabel.setVisibility(View.GONE);
        mMovieCoverImage.setVisibility(View.GONE);

        mLoadingContainer1.setVisibility(View.GONE);
        mLoadingContainer2.setVisibility(View.GONE);

        mErrorContainer1.setVisibility(View.VISIBLE);
        mErrorContainer2.setVisibility(View.VISIBLE);
    }

    private void subscribe(Observable<RestResponse<Movie>> observable) {
        if (mSchedule != null && mSchedule.getMovie() != null) {
            Movie movie = mSchedule.getMovie();
            showContent(movie.getCoverUrl(), movie.getMovieName());
            return;
        }

        showLoading();

        unsubscribe();

        mMovieSubscriber = new Subscriber<RestResponse<Movie>>() {
            @Override
            public void onCompleted() {
                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                showError();
                unsubscribe();
            }

            @Override
            public void onNext(RestResponse<Movie> response) {
                Movie movie = response.getResponse();
                if (mSchedule != null) {
                    mSchedule.setMovie(movie);
                }

                showContent(movie.getCoverUrl(), movie.getMovieName());
            }
        };

        observable
                .delay(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mMovieSubscriber);

    }


}
