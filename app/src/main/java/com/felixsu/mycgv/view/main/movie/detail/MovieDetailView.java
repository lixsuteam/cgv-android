package com.felixsu.mycgv.view.main.movie.detail;

import com.felixsu.common.model.cinema.Schedule;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.ArrayList;

/**
 * Created on 12/20/16.
 *
 * @author felixsoewito
 */

public interface MovieDetailView extends MvpView {

    void showContent();
    void showError();
    void showLoading();

    void setContent(ArrayList<ArrayList<Schedule>> schedules);
    void setError(String message);

    ArrayList<ArrayList<Schedule>> getContent();
    String getError();
}
