package com.felixsu.mycgv.view.main.movie.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.fragment.BaseViewStateFragment;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.view.main.MainActivity;
import com.felixsu.mycgv.view.main.movie.collection.adapter.ComingSoonAdapter;
import com.felixsu.mycgv.view.main.movie.collection.adapter.NowPlayingAdapter;
import com.felixsu.mycgv.view.main.movie.collection.dagger.DaggerMovieCollectionComponent;
import com.felixsu.mycgv.view.main.movie.collection.dagger.MovieCollectionComponent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 */

public class MovieCollectionFragment
        extends BaseViewStateFragment<MovieCollectionView, MovieCollectionPresenter, MovieCollectionViewState>
        implements MovieCollectionView {

    public static final String TAG = MovieCollectionFragment.class.getName();

    //VIEW
    //error
    @BindView(R.id.container_nowPlayingError)
    RelativeLayout mNowPlayingErrorContainer;
    @BindView(R.id.container_comingSoonError)
    RelativeLayout mComingSoonErrorContainer;

    //loading
    @BindView(R.id.container_nowPlayingLoading)
    RelativeLayout mNowPlayingLoadingContainer;
    @BindView(R.id.container_comingSoonLoading)
    RelativeLayout mComingSoonLoadingContainer;
    @BindView(R.id.progress_nowPlaying)
    ProgressBar mNowPLayingProgressBar;
    @BindView(R.id.progress_comingSoon)
    ProgressBar mComingSoonProgressBar;

    //content
    @BindView(R.id.container_nowPlayingContent)
    RecyclerView mNowPlayingContentContainer;
    @BindView(R.id.container_comingSoonContent)
    RecyclerView mComingSoonContentContainer;

    //DATA
    @Inject SharedData mSharedData;
    MovieCollectionComponent mComponent;
    String mNowPlayingErrorMessage;
    String mComingSoonErrorMessage;

    //ADAPTER
    NowPlayingAdapter mNowPlayingAdapter;
    ComingSoonAdapter mComingSoonAdapter;

    //LISTENER
    ListAdapter.ItemClickListener mNowPlayingClickListener;

    ListAdapter.ItemClickListener mComingSoonClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNowPlayingClickListener = new ListAdapter.ItemClickListener() {
            @Override
            public void onClick(View v, int i) {
                if (mNowPlayingContentContainer != null){
                    RecyclerView.Adapter adapter = mNowPlayingContentContainer.getAdapter();

                    if (adapter instanceof ListAdapter){
                        Movie m = (Movie) ((ListAdapter) adapter).getItem(i);
                        ((MainActivity)getActivity()).openMovie(m, TAG);
                    } else {
                        throw new RuntimeException("Adapter not instance of SimpleRecyclerViewAdapter");
                    }
                }
            }
        };
        mNowPlayingAdapter = new NowPlayingAdapter(getContext(), getNowPlaying());
        mNowPlayingAdapter.setItemClickListener(mNowPlayingClickListener);

        mComingSoonClickListener = new ListAdapter.ItemClickListener() {
            @Override
            public void onClick(View v, int i) {
                if (mComingSoonContentContainer != null){
                    RecyclerView.Adapter adapter = mComingSoonContentContainer.getAdapter();

                    if (adapter instanceof ListAdapter){
                        Movie m = (Movie) ((ListAdapter) adapter).getItem(i);
                        if (m.getMovieName() == null || m.getMovieName().isEmpty()) {
                            Toast.makeText(getContext(), R.string.text_movie_param_not_complete, Toast.LENGTH_SHORT).show();
                        } else {
                            ((MainActivity)getActivity()).openMovie(m, TAG);
                        }
                    } else {
                        throw new RuntimeException("Adapter not instance of SimpleRecyclerViewAdapter");
                    }
                }
            }
        };
        mComingSoonAdapter = new ComingSoonAdapter(getContext(), getComingSoon());
        mComingSoonAdapter.setItemClickListener(mComingSoonClickListener);

        mNowPlayingContentContainer.setAdapter(mNowPlayingAdapter);
        mNowPlayingContentContainer.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        mComingSoonContentContainer.setAdapter(mComingSoonAdapter);
        mComingSoonContentContainer.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void onDestroyView() {
        mNowPlayingClickListener = null;
        mNowPlayingAdapter = null;

        mComingSoonClickListener = null;
        mComingSoonAdapter = null;
        super.onDestroyView();
    }

    @NonNull
    @Override
    public MovieCollectionViewState createViewState() {
        return new MovieCollectionViewState();
    }

    @NonNull
    @Override
    public MovieCollectionPresenter createPresenter() {
        return mComponent.presenter();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.load();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_movie_collection;
    }

    @Override
    public void showContentNowPlaying() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setNowPlayingContent(getNowPlaying());

        mNowPlayingLoadingContainer.setVisibility(View.GONE);
        mNowPlayingErrorContainer.setVisibility(View.GONE);
        mNowPlayingContentContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorNowPlaying() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setNowPlayingError(getErrorNowPlaying());

        mNowPlayingLoadingContainer.setVisibility(View.GONE);
        mNowPlayingContentContainer.setVisibility(View.GONE);
        mNowPlayingErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingNowPlaying() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setNowPlayingLoading();

        mNowPlayingContentContainer.setVisibility(View.GONE);
        mNowPlayingErrorContainer.setVisibility(View.GONE);
        mNowPlayingLoadingContainer.setVisibility(View.VISIBLE);

    }

    @Override
    public void showContentComingSoon() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setComingSoonContent(getComingSoon());

        mComingSoonLoadingContainer.setVisibility(View.GONE);
        mComingSoonErrorContainer.setVisibility(View.GONE);
        mComingSoonContentContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorComingSoon() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setComingSoonError(getErrorComingSoon());

        mComingSoonLoadingContainer.setVisibility(View.GONE);
        mComingSoonContentContainer.setVisibility(View.GONE);
        mComingSoonErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingComingSoon() {
        MovieCollectionViewState vs = (MovieCollectionViewState) getViewState();
        vs.setComingSoonLoading();

        mComingSoonContentContainer.setVisibility(View.GONE);
        mComingSoonErrorContainer.setVisibility(View.GONE);
        mComingSoonLoadingContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void setNowPlaying(List<Movie> movies) {
        mNowPlayingAdapter.setItems(movies);
    }

    @Override
    public void setComingSoon(List<Movie> movies) {
        mComingSoonAdapter.setItems(movies);
    }

    @Override
    public void setErrorNowPlaying(String message) {
        mNowPlayingErrorMessage = message;
    }

    @Override
    public void setErrorComingSoon(String message) {
        mComingSoonErrorMessage = message;
    }

    @Override
    public List<Movie> getNowPlaying() {
        return mNowPlayingAdapter != null ? mNowPlayingAdapter.getItems() : null;
    }

    @Override
    public List<Movie> getComingSoon() {
        return mComingSoonAdapter != null ? mComingSoonAdapter.getItems() : null;
    }

    @Override
    public String getErrorNowPlaying() {
        return mNowPlayingErrorMessage;
    }

    @Override
    public String getErrorComingSoon() {
        return mComingSoonErrorMessage;
    }

    @Override
    protected void injectDependencies() {
        mComponent = DaggerMovieCollectionComponent.builder()
                .cgvAppComponent(getBaseActivity().getBaseApplication().getComponent())
                .build();
        mComponent.inject(this);
    }

    @OnClick(R.id.button_now_playing_refresh)
    public void reloadNowPlaying() {
        presenter.loadNowPlaying();
    }

    @OnClick(R.id.button_coming_soon_refresh)
    public void reloadComingSoon() {
        presenter.loadComingSoon();
    }

}
