package com.felixsu.mycgv.view.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.dagger.component.ActivityComponent;
import com.felixsu.mycgv.common.dagger.component.DaggerActivityComponent;
import com.felixsu.mycgv.common.view.activity.BaseActivity;
import com.felixsu.mycgv.helper.CinemaHelper;
import com.felixsu.mycgv.helper.FirebaseHelper;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.view.main.cinema.collection.CinemaCollectionFragment;
import com.felixsu.mycgv.view.main.cinema.detail.CinemaDetailFragment;
import com.felixsu.mycgv.view.main.cinema.detail.CinemaDetailFragmentBuilder;
import com.felixsu.mycgv.view.main.movie.collection.MovieCollectionFragment;
import com.felixsu.mycgv.view.main.movie.detail.MovieDetailFragmentBuilder;
import com.felixsu.mycgv.view.main.movie.passive.MovieDetailPassiveFragmentBuilder;
import com.felixsu.mycgv.view.wizard.WizardActivity;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import icepick.State;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 *
 */
public class MainActivity extends BaseActivity
        implements OnTabSelectListener, OnTabReselectListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int INDEX_MOVIE = MainFragmentAdapter.TAB1;
    private static final int INDEX_CINEMA = MainFragmentAdapter.TAB2;

    @BindView(R.id.app_bottom_bar)
    BottomBar mBottomBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    SharedData mSharedData;
    private FirebaseAnalytics mFirebaseAnalytics;

    @State String mCity;
    @State Integer mActiveTab;
    @State String mMovieDetailsCaller;

    private MainFragmentAdapter mFragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get persisted city unless icepick get it, otherwise refresh city on shared data
        if (mCity == null || mCity.isEmpty()) {
            mCity = mSharedData.getCity();
        } else {
            mSharedData.putCity(mCity);
        }

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(String.format("My CGV - %s", mCity));
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //setup fragment
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new MovieCollectionFragment());
        fragmentList.add(new CinemaCollectionFragment());

        mActiveTab = mActiveTab == null ? INDEX_MOVIE : mActiveTab;
        mFragmentAdapter = new MainFragmentAdapter(savedInstanceState,
                getSupportFragmentManager(), R.id.container_main, fragmentList, mActiveTab);
        mFragmentAdapter.setTransitionMode(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        mBottomBar.setOnTabSelectListener(this);
        mBottomBar.setOnTabReselectListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mFragmentAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_about:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getString(R.string.text_about));
                alertDialog.setMessage(getString(R.string.text_about_message));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
                return true;
            case R.id.action_select_city:
                Intent intent = new Intent(this, WizardActivity.class);
                intent.putExtra(MainActivity.TAG, mCity);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabReSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_movies:
            case R.id.tab_cinemas:
                mFragmentAdapter.clearStack();
                break;
            default:
                break;
        }
    }

    @Override
    public void onTabSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_movies:
                mMovieDetailsCaller = MovieCollectionFragment.TAG;
                mActiveTab = INDEX_MOVIE;
                mFragmentAdapter.switchTab(INDEX_MOVIE);
                break;
            case R.id.tab_cinemas:
                mActiveTab = INDEX_CINEMA;
                mFragmentAdapter.switchTab(INDEX_CINEMA);
                break;
            default:
                break;
        }
    }

    @Override
    protected void injectDependencies() {
        ActivityComponent component = DaggerActivityComponent.builder()
                .cgvAppComponent(getBaseApplication().getComponent())
                .build();
        component.inject(this);
    }

    @Override
    public void onBackPressed() {
        if (mFragmentAdapter.getCurrentStack().size() > 1) {
            if (mMovieDetailsCaller.equals(CinemaDetailFragment.TAG)) {
                mBottomBar.selectTabAtPosition(INDEX_CINEMA);
                mActiveTab = INDEX_CINEMA;
                mFragmentAdapter.switchTab(INDEX_CINEMA);
            } else {
                mFragmentAdapter.pop();
            }
        } else {
            super.onBackPressed();
        }
    }

    public void openMovie(Movie m, String caller) {
        if (m == null) {
            Log.i(TAG, "clicked movie is null, returning");
            return;
        }

        FirebaseHelper.logEventOpenMovie(mFirebaseAnalytics, m);
        final String DURATION = m.getDuration().toString().concat(" min");
        final String TOKEN = "#";

        final String ACTORS = m.getActors().replaceAll(TOKEN, ", ");
        final String description = m.getMovieDescription().isEmpty() ? getString(R.string.text_not_available) : m.getMovieDescription();

        Fragment fragment;

        if (m.getStatus().equals(Movie.Status.STATUS_NOW_PLAYING)) {
            fragment = new MovieDetailFragmentBuilder(ACTORS,
                    m.getCoverUrl(), description, m.getDirectors(),
                    DURATION, m.getGenre(), m.getId(), m.getLanguage(),
                    m.getRating(), m.getMovieName(), m.getTrailerUrl()).build();
        } else {
            fragment = new MovieDetailPassiveFragmentBuilder(ACTORS,
                    m.getCoverUrl(), description, m.getDirectors(),
                    DURATION, m.getGenre(), m.getId(), m.getLanguage(),
                    m.getRating(), m.getMovieName(), m.getTrailerUrl()).build();
        }

        if (mBottomBar.getCurrentTabPosition() != INDEX_MOVIE) {
            mBottomBar.selectTabAtPosition(INDEX_MOVIE);
            mActiveTab = INDEX_MOVIE;
            mFragmentAdapter.switchTab(INDEX_MOVIE);
        }
        //assignemnt happened after tab selected
        mMovieDetailsCaller = caller;

        if (mFragmentAdapter.getCurrentStack().size() > 1) {
            mFragmentAdapter.pop();
        }
        mFragmentAdapter.push(fragment);
    }

    public void openCinema(Cinema c) {
        FirebaseHelper.logEventOpenCinema(mFirebaseAnalytics, c);
        if (CinemaHelper.isValidCinema(c)) {
            final String[] address = CinemaHelper.findAddress(c.getCinemaDetails());
            final String primaryAddress = address[0];
            final String secondaryAddress = address[1];
            final String features = CinemaHelper.findFeatures(c.getCinemaDetails());
            final String phoneNumbers = CinemaHelper.findPhoneNumbers(c.getCinemaDetails());
            CinemaDetailFragment fragment = new CinemaDetailFragmentBuilder(features, c.getId(), c.getCinemaName(),
                    phoneNumbers, primaryAddress, secondaryAddress)
                    .build();

            mFragmentAdapter.push(fragment);
        } else {
            Toast.makeText(this, R.string.text_cinema_not_supported, Toast.LENGTH_SHORT).show();
        }
    }
}
