package com.felixsu.mycgv.view.main.movie.detail.dagger;

import com.felixsu.mycgv.common.dagger.PerFragment;
import com.felixsu.mycgv.common.dagger.component.CgvAppComponent;
import com.felixsu.mycgv.view.main.movie.detail.MovieDetailFragment;
import com.felixsu.mycgv.view.main.movie.detail.MovieDetailPresenter;

import dagger.Component;

/**
 * Created by felixsoewito on 12/20/16.
 */

@PerFragment
@Component(
        dependencies = CgvAppComponent.class
)
public interface MovieDetailComponent {

    MovieDetailPresenter presenter();

    void inject(MovieDetailFragment fragment);
}
