package com.felixsu.mycgv.view.main.movie.collection;

import android.util.Log;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public class MovieCollectionPresenter extends MvpBasePresenter<MovieCollectionView> {

    private static final String TAG = MovieCollectionPresenter.class.getName();

    private static final long MOVIES_VALIDITY = 60*60*1000; //1hour

    private Subscriber<RestResponse<Movie>> mNowPlayingSubscriber;
    private Subscriber<RestResponse<Movie>> mComingSoonSubscriber;

    private CgvService mCgvService;
    private SharedData mSharedData;

    @Inject
    public MovieCollectionPresenter(CgvService cgvService, SharedData sharedData) {
        mCgvService = cgvService;
        mSharedData = sharedData;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            cancelNowPlayingSubscription();
            cancelComingSoonSubscription();
        }
    }

    public void load() {
        loadNowPlaying();
        loadComingSoon();
    }

    public void loadNowPlaying() {
        subscribeNowPlaying(mCgvService.getMovies(null, null, mSharedData.getCity(), Cinema.CGV));
    }

    public void loadComingSoon() {
        subscribeComingSoon(mCgvService.getMovies("coming_soon", null, null, Cinema.CGV));
    }

    private void subscribeNowPlaying(Observable<RestResponse<Movie>> observable) {
        long currentTime = new Date().getTime();
        long lastStore = mSharedData.getNowPlayingSynchronized() == null ? 0L : mSharedData.getNowPlayingSynchronized();

        boolean isNowPlayingValid = (currentTime - lastStore) < MOVIES_VALIDITY;
        boolean isLoaded = mSharedData.getNowPlayingContent() != null;

        if (isLoaded && isNowPlayingValid) {
            if (isViewAttached()) {
                getView().setNowPlaying(mSharedData.getNowPlayingContent());
                getView().showContentNowPlaying();
            }
            return;
        }

        if (isViewAttached()) {
            getView().showLoadingNowPlaying();
        }

        cancelNowPlayingSubscription();

        mNowPlayingSubscriber = new Subscriber<RestResponse<Movie>>() {
            @Override
            public void onCompleted() {
                if (isViewAttached()) {
                    getView().showContentNowPlaying();
                }

                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                if (isViewAttached()) {
                    getView().showErrorNowPlaying();
                }
                unsubscribe();
            }

            @Override
            public void onNext(RestResponse<Movie> response) {
                List<Movie> movies = response.getResponses();

                mSharedData.putNowPlayingSynchronized(new Date().getTime());
                mSharedData.putNowPlayingContent(movies);

                if (isViewAttached()) {
                    getView().setNowPlaying(movies);
                }
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mNowPlayingSubscriber);

    }

    private void subscribeComingSoon(Observable<RestResponse<Movie>> observable) {

        boolean isLoaded = mSharedData.getComingSoonContent() != null;

        if (isLoaded) {
            if (isViewAttached()) {
                getView().setComingSoon(mSharedData.getComingSoonContent());
                getView().showContentComingSoon();
            }
            return;
        }

        if (isViewAttached()) {
            getView().showLoadingComingSoon();
        }

        cancelComingSoonSubscription();

        mComingSoonSubscriber = new Subscriber<RestResponse<Movie>>() {
            @Override
            public void onCompleted() {
                if (isViewAttached()) {
                    getView().showContentComingSoon();
                }
                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                if (isViewAttached()) {
                    getView().showErrorComingSoon();
                }
                unsubscribe();
            }

            @Override
            public void onNext(RestResponse<Movie> response) {
                List<Movie> movies = response.getResponses();

                mSharedData.putComingSoonContent(movies);

                if (isViewAttached()) {
                    getView().setComingSoon(movies);
                }
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mComingSoonSubscriber);
    }

    private void cancelNowPlayingSubscription() {
        if (mNowPlayingSubscriber != null && !mNowPlayingSubscriber.isUnsubscribed()) {
            mNowPlayingSubscriber.unsubscribe();
        }
        mNowPlayingSubscriber = null;
    }

    private void cancelComingSoonSubscription() {
        if (mComingSoonSubscriber != null && !mComingSoonSubscriber.isUnsubscribed()) {
            mComingSoonSubscriber.unsubscribe();
        }
        mComingSoonSubscriber = null;
    }
}
