package com.felixsu.mycgv.view.main.movie.detail.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.felixsu.mycgv.common.view.adapter.ListViewHolder;
import com.felixsu.mycgv.helper.CinemaHelper;

/**
 * Created on 12/21/16.
 *
 * @author felixsoewito
 */

public class ScheduleSecondChildViewHolder extends ListViewHolder<String> {

    public static final String TAG = ScheduleSecondChildViewHolder.class.getName();

    private final Context mContext;
    private Schedule mSchedule;
    private TextView[] mContainer;

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView tv = (TextView) v;
            String text = tv.getText().toString();

            if (!text.equals("-")) {
                final String URL = CinemaHelper.generateReservation(mSchedule, tv.getText().toString());

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder
                        .setTitle("Open Reservation")
                        .setMessage("Are you sure to open reservation for this movie on " + text)
                        .setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(URL));
                                mContext.startActivity(i);
                                dialog.dismiss();
                            }
                        })

                        .setNegativeButton(R.string.text_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                builder.create().show();
            }
        }
    };

    public ScheduleSecondChildViewHolder(View itemView, Schedule schedule, Context context) {
        super(itemView);
        mSchedule = schedule;
        mContext = context;

        TextView scheduleLabel1 = (TextView) itemView.findViewById(R.id.label_value_1);
        TextView scheduleLabel2 = (TextView) itemView.findViewById(R.id.label_value_2);
        TextView scheduleLabel3 = (TextView) itemView.findViewById(R.id.label_value_3);
        TextView scheduleLabel4 = (TextView) itemView.findViewById(R.id.label_value_4);

        mContainer = new TextView[]{scheduleLabel1, scheduleLabel2, scheduleLabel3, scheduleLabel4};
        for (TextView tv : mContainer) {
            tv.setOnClickListener(mClickListener);
            tv.setClickable(true);
        }
    }

    @Override
    public void bindView(String item) {
        throw new RuntimeException(this.getClass().getName() + " should use other implementation");
    }

    public void bindView(String item, Schedule schedule) {
        mSchedule = schedule;

        String[] children = item.split("#");
        int contentSize = children.length;

        for (int i = 0; i < 4; i++) {
            if (i < contentSize) {
                mContainer[i].setText(children[i]);
            } else {
                mContainer[i].setText("-");
            }
        }
    }
}
