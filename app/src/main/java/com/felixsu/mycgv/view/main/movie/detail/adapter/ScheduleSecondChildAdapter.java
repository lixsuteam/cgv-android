package com.felixsu.mycgv.view.main.movie.detail.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;

import java.util.List;

/**
 * Created on 12/21/16.
 *
 * @author felixsoewito
 */

public class ScheduleSecondChildAdapter extends ListAdapter<String, ScheduleSecondChildViewHolder> {

    private final Schedule mSchedule;
    private final Context mContext;

    public ScheduleSecondChildAdapter(@NonNull Context context, @NonNull Schedule schedule, @NonNull List<String> items) {
        super(items);
        mSchedule = schedule;
        mContext = context;
    }

    @Override
    public ScheduleSecondChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_schedule_second_child, parent, false);
        return new ScheduleSecondChildViewHolder(view, mSchedule, mContext);
    }

    @Override
    public void onBindViewHolder(ScheduleSecondChildViewHolder holder, int position) {
        holder.bindView(getItem(position), mSchedule);
    }
}
