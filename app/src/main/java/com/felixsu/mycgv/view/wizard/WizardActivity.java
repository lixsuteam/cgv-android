package com.felixsu.mycgv.view.wizard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.dagger.component.ActivityComponent;
import com.felixsu.mycgv.common.dagger.component.DaggerActivityComponent;
import com.felixsu.mycgv.helper.FirebaseHelper;
import com.felixsu.mycgv.provider.data.local.PersistanceHelper;
import com.felixsu.mycgv.common.view.activity.BaseActivity;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.view.main.MainActivity;
import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 */

public class WizardActivity extends BaseActivity {

    private static final String[] mCities = new String[]{
            "Jakarta","BalikPapan", "Bandung", "Batam", "Bekasi", "Cirebon", "Depok", "Karawang",
            "Manado", "Mataram", "Medan", "Mojokerto", "Palembang", "Pekanbaru", "Purwokerto", "Surabaya",
            "Surabaya", "Tangerang", "Tegal", "Yogyakarta"
    };

    @BindView(R.id.lv_city_subscription)
    ListView mCitySubscription;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    SharedPreferences mSharedPreferences;

    @Inject
    SharedData mSharedData;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);

        setSupportActionBar(mToolbar);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //setup back button if city available
        String city = getIntent().getStringExtra(MainActivity.TAG);
        if (city != null) {
            ActionBar actionBar = getSupportActionBar();

            assert actionBar != null;

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        //setup list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mCities);

        mCitySubscription.setAdapter(adapter);
        mCitySubscription.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCity = (String) mCitySubscription.getItemAtPosition(position);

                FirebaseHelper.logEventSelectCity(mFirebaseAnalytics, selectedCity);
                PersistanceHelper.putCity(mSharedPreferences.edit(), selectedCity);
                mSharedData.putCity(selectedCity);
                mSharedData.putNowPlayingContent(null);
                mSharedData.putCinemaContent(null);

                Intent intent = new Intent(WizardActivity.this, MainActivity.class);
                intent
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void injectDependencies() {
        ActivityComponent component = DaggerActivityComponent.builder()
                .cgvAppComponent(getBaseApplication().getComponent())
                .build();
        component.inject(this);
    }
}
