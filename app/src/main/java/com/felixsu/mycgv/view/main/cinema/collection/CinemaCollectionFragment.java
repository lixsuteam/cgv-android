package com.felixsu.mycgv.view.main.cinema.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.fragment.BaseFragment;
import com.felixsu.mycgv.common.view.fragment.BaseViewStateFragment;
import com.felixsu.mycgv.helper.CinemaHelper;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.view.main.MainActivity;
import com.felixsu.mycgv.view.main.cinema.collection.adapter.CinemaAdapter;
import com.felixsu.mycgv.view.main.cinema.collection.dagger.CinemaCollectionComponent;
import com.felixsu.mycgv.view.main.cinema.collection.dagger.DaggerCinemaCollectionComponent;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import com.robohorse.pagerbullet.PagerBullet;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created on 12/17/16.
 *
 * @author felixsoewito
 */

public class CinemaCollectionFragment
        extends BaseViewStateFragment<CinemaCollectionView, CinemaCollectionPresenter, CinemaCollectionViewState>
        implements CinemaCollectionView, CinemaAdapter.CinemaClickListener {

    private static final String TAG = CinemaCollectionFragment.class.getName();

    //VIEW
    //error
    @BindView(R.id.container_cinemaError)
    RelativeLayout mCinemaErrorContainer;

    //content
    @BindView(R.id.container_cinemaContent)
    RelativeLayout mCinemaContentContainer;
    @BindView(R.id.container_cinemas)
    PagerBullet mCinemasContainer;

    //loading
    @BindView(R.id.container_cinemaLoading)
    RelativeLayout mCinemaLoadingContainer;

    //DATA
    CinemaCollectionComponent mComponent;
    String mErrorMessage;

    //ADAPTER
    CinemaAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCinemasContainer.setIndicatorTintColorScheme(
                getContext().getResources().getColor(R.color.colorAccent),
                getContext().getResources().getColor(R.color.colorPrimary));
        mCinemasContainer.getViewPager().setClipToPadding(false);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int paddingPx = (int) (dm.density * 32);
        int halfPaddingPx = (int) (dm.density * 4);
        Log.i(TAG, "current density =>" + dm.density);
        mCinemasContainer.getViewPager().setPadding(paddingPx, 0, paddingPx, 0);
        mCinemasContainer.getViewPager().setPageMargin(halfPaddingPx);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        mAdapter = null;
        super.onDestroyView();
    }

    @Override
    public void showContent() {
        CinemaCollectionViewState vs = getViewState();
        vs.setContent(getContent());

        mCinemaContentContainer.setVisibility(View.VISIBLE);
        mCinemaLoadingContainer.setVisibility(View.GONE);
        mCinemaErrorContainer.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        CinemaCollectionViewState vs = getViewState();
        vs.setError();

        mCinemaContentContainer.setVisibility(View.GONE);
        mCinemaLoadingContainer.setVisibility(View.GONE);
        mCinemaErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        CinemaCollectionViewState vs = getViewState();
        vs.setLoading();

        mCinemaContentContainer.setVisibility(View.GONE);
        mCinemaLoadingContainer.setVisibility(View.VISIBLE);
        mCinemaErrorContainer.setVisibility(View.GONE);
    }

    @Override
    public void setContent(List<Cinema> cinemas) {
        if (mCinemasContainer.getViewPager().getAdapter() != null) {
            ((CinemaAdapter) mCinemasContainer.getViewPager().getAdapter()).setCinemas(cinemas);
            mCinemasContainer.invalidateBullets();
        } else {
            mAdapter = new CinemaAdapter(cinemas, getContext(), this);
            mCinemasContainer.setAdapter(mAdapter);
        }
    }

    @Override
    public void setError(String message) {
        mErrorMessage = message;
    }

    @Override
    public List<Cinema> getContent() {
        return mAdapter.getCinemas();
    }

    @Override
    public String getError() {
        return mErrorMessage;
    }

    @NonNull
    @Override
    public CinemaCollectionViewState createViewState() {
        return new CinemaCollectionViewState();
    }

    @NonNull
    @Override
    public CinemaCollectionPresenter createPresenter() {
        return mComponent.presenter();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.load();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_cinema_collection;
    }

    @Override
    protected void injectDependencies() {
        mComponent = DaggerCinemaCollectionComponent.builder()
                .cgvAppComponent(getBaseActivity().getBaseApplication().getComponent())
                .build();
        mComponent.inject(this);
    }

    @Override
    public void onItemClick(View v, Cinema cinema) {
        if (CinemaHelper.isValidCinema(cinema)) {
            ((MainActivity) getActivity()).openCinema(cinema);
        }
    }

    @OnClick(R.id.button_cinema_refresh)
    public void refreshCinema() {
        presenter.load();
    }


}
