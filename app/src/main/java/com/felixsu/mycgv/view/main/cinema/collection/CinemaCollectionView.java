package com.felixsu.mycgv.view.main.cinema.collection;

import com.felixsu.common.model.cinema.Cinema;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public interface CinemaCollectionView extends MvpView {

    void showContent();
    void showError();
    void showLoading();

    void setContent(List<Cinema> cinemas);
    void setError(String message);

    List<Cinema> getContent();
    String getError();
}
