package com.felixsu.mycgv.view.main.cinema.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.fragment.BaseViewStateFragment;
import com.felixsu.mycgv.view.main.MainActivity;
import com.felixsu.mycgv.view.main.cinema.detail.adapter.ScheduleParentAdapter;
import com.felixsu.mycgv.view.main.cinema.detail.dagger.CinemaDetailComponent;
import com.felixsu.mycgv.view.main.cinema.detail.dagger.DaggerCinemaDetailComponent;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * created on 12/24/16.
 *
 * @author felixsoewito
 */

public class CinemaDetailFragment
        extends BaseViewStateFragment<CinemaDetailView, CinemaDetailPresenter, CinemaDetailViewState>
        implements CinemaDetailView, ListAdapter.ItemClickListener {

    public static final String TAG = CinemaDetailFragment.class.getName();

    //VIEW
    //static
    @BindView(R.id.label_cinema_name)
    TextView mNameLabel;
    @BindView(R.id.label_cinema_address)
    TextView mPrimaryAddressLabel;
    @BindView(R.id.label_cinema_address_2)
    TextView mSecondaryAddressLabel;
    @BindView(R.id.label_phone)
    TextView mPhoneLabel;
    @BindView(R.id.label_features)
    TextView mFeaturesLabel;
    @BindView(R.id.container_schedule)
    RecyclerView mScheduleContainer;

    //Loading
    @BindView(R.id.container_loading) View mLoadingContainer;
    //Content
    @BindView(R.id.container_content) View mContentContainer;
    //Error
    @BindView(R.id.container_error) View mErrorContainer;

    //DATA
    private CinemaDetailComponent mComponent;
    private String mErrorMessage;
    @Arg String mId;
    @Arg String mName;
    @Arg String mPrimaryAddress;
    @Arg String mSecondaryAddress;
    @Arg String mPhone;
    @Arg String mFeatures;

    private ScheduleParentAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNameLabel.setText(mName);
        mPrimaryAddressLabel.setText(mPrimaryAddress);
        mSecondaryAddressLabel.setText(mSecondaryAddress);
        mPhoneLabel.setText(mPhone);
        mFeaturesLabel.setText(mFeatures);

        mAdapter = new ScheduleParentAdapter(getContext(), mComponent.cgvService(), getContent());
        mAdapter.setItemClickListener(this);

        mScheduleContainer.setAdapter(mAdapter);
        mScheduleContainer.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mScheduleContainer.setNestedScrollingEnabled(false);
        mScheduleContainer.setHasFixedSize(true);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdapter = null;
    }
    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_cinema_detail;
    }

    @NonNull
    @Override
    public CinemaDetailViewState createViewState() {
        return new CinemaDetailViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.load(mId);
    }

    @NonNull
    @Override
    public CinemaDetailPresenter createPresenter() {
        return mComponent.presenter();
    }

    @Override
    protected void injectDependencies() {
        mComponent = DaggerCinemaDetailComponent.builder()
                .cgvAppComponent(getBaseActivity().getBaseApplication().getComponent())
                .build();
        mComponent.inject(this);
    }

    @Override
    public void showContent() {
        CinemaDetailViewState vs = (CinemaDetailViewState) getViewState();
        vs.setContent(getContent());

        mLoadingContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        CinemaDetailViewState vs = (CinemaDetailViewState) getViewState();
        vs.setError();

        mLoadingContainer.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        CinemaDetailViewState vs = (CinemaDetailViewState) getViewState();
        vs.setLoading();

        mErrorContainer.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
        mLoadingContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void setContent(ArrayList<ArrayList<Schedule>> schedules) {
        mAdapter.setItems(schedules);
    }

    @Override
    public void setError(String message) {
        mErrorMessage = message;
    }

    @Override
    public ArrayList<ArrayList<Schedule>> getContent() {
        return mAdapter!=null ? (ArrayList<ArrayList<Schedule>>) mAdapter.getItems() : null;
    }

    @Override
    public String getError() {
        return mErrorMessage;
    }

    @Override
    public void onClick(View v, int i) {
        if (mAdapter != null) {
            ArrayList<Schedule> schedules = mAdapter.getItem(i);
            if (!schedules.isEmpty()) {
                Movie m = schedules.get(0).getMovie();
                ((MainActivity)getActivity()).openMovie(m, TAG);
            }
        }
    }
}
