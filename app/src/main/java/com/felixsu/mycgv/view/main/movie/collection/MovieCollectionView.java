package com.felixsu.mycgv.view.main.movie.collection;

import com.felixsu.common.model.cinema.Movie;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public interface MovieCollectionView extends MvpView {

    void showContentNowPlaying();
    void showErrorNowPlaying();
    void showLoadingNowPlaying();

    void showContentComingSoon();
    void showErrorComingSoon();
    void showLoadingComingSoon();

    void setNowPlaying(List<Movie> movies);
    void setComingSoon(List<Movie> movies);

    void setErrorNowPlaying(String message);
    void setErrorComingSoon(String message);

    List<Movie> getNowPlaying();
    List<Movie> getComingSoon();

    String getErrorNowPlaying();
    String getErrorComingSoon();
}
