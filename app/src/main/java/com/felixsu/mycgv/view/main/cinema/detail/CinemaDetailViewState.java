package com.felixsu.mycgv.view.main.cinema.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.felixsu.common.model.cinema.Schedule;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;

import java.util.ArrayList;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

public class CinemaDetailViewState implements RestorableViewState<CinemaDetailView> {

    private static final String KEY_STATE = "key-state";
    private static final String KEY_CONTENT = "key-content";

    private final int SHOW_SCHEDULE_CONTENT = 0;
    private final int SHOW_SCHEDULE_LOADING = 1;
    private final int SHOW_SCHEDULE_ERROR = 2;

    private ArrayList<ArrayList<Schedule>> mContent;
    private Integer mState = SHOW_SCHEDULE_LOADING;

    void setContent(ArrayList<ArrayList<Schedule>> content) {
        mContent = content;
        mState = SHOW_SCHEDULE_CONTENT;
    }

    void setLoading() {
        mState = SHOW_SCHEDULE_LOADING;
    }

    void setError() {
        mState = SHOW_SCHEDULE_ERROR;
    }

    @Override
    public void saveInstanceState(@NonNull Bundle out) {
        out.putInt(KEY_STATE, mState);
        out.putSerializable(KEY_CONTENT, mContent);
    }

    @SuppressWarnings("unchecked")
    @Override
    public RestorableViewState<CinemaDetailView> restoreInstanceState(Bundle in) {
        if (in == null){
            return null;
        }
        mState = in.getInt(KEY_STATE);
        mContent = (ArrayList<ArrayList<Schedule>>) in.getSerializable(KEY_CONTENT);
        return this;
    }

    @Override
    public void apply(CinemaDetailView view, boolean retained) {
        switch (mState) {
            case SHOW_SCHEDULE_CONTENT:
                view.setContent(mContent);
                view.showContent();
                break;
            case SHOW_SCHEDULE_LOADING:
                view.showLoading();
                break;
            case SHOW_SCHEDULE_ERROR:
                view.showError();
                break;
            default:
                view.showError();
                break;
        }
    }
}
