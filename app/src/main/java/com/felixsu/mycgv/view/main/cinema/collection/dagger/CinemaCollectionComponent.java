package com.felixsu.mycgv.view.main.cinema.collection.dagger;

import com.felixsu.mycgv.common.dagger.PerFragment;
import com.felixsu.mycgv.common.dagger.component.CgvAppComponent;
import com.felixsu.mycgv.view.main.cinema.collection.CinemaCollectionFragment;
import com.felixsu.mycgv.view.main.cinema.collection.CinemaCollectionPresenter;

import dagger.Component;

/**
 * Created on
 *
 * @author felixsoewito on 12/19/16.
 */

@PerFragment
@Component(
        dependencies = CgvAppComponent.class
)
public interface CinemaCollectionComponent {

    CinemaCollectionPresenter presenter();

    void inject(CinemaCollectionFragment fragment);

}
