package com.felixsu.mycgv.view.main.cinema.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.felixsu.common.model.cinema.Cinema;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/19/16.
 *
 * @author felixsoewito
 */

public class CinemaCollectionViewState implements RestorableViewState<CinemaCollectionView> {

    private static final String KEY_STATE = "key-state";
    private static final String KEY_CONTENT = "key-content";

    private final int SHOW_CINEMA_CONTENT = 0;
    private final int SHOW_CINEMA_LOADING = 1;
    private final int SHOW_CINEMA_ERROR = 2;

    private int mState = SHOW_CINEMA_LOADING;
    private ArrayList<Cinema> mCinemas;

    void setContent(List<Cinema> cinemas) {
        mCinemas = (ArrayList<Cinema>) cinemas;
        mState = SHOW_CINEMA_CONTENT;
    }

    void setLoading() {
        mState = SHOW_CINEMA_LOADING;
    }

    void setError() {
        mState = SHOW_CINEMA_ERROR;
    }

    @Override
    public void saveInstanceState(@NonNull Bundle out) {
        out.putInt(KEY_STATE, mState);
        out.putSerializable(KEY_CONTENT, mCinemas);
    }

    @SuppressWarnings("unchecked")
    @Override
    public RestorableViewState<CinemaCollectionView> restoreInstanceState(Bundle in) {
        if (in == null){
            return null;
        }
        mState = in.getInt(KEY_STATE);
        mCinemas = (ArrayList<Cinema>) in.getSerializable(KEY_CONTENT);
        return this;
    }

    @Override
    public void apply(CinemaCollectionView view, boolean retained) {
        switch (mState) {
            case SHOW_CINEMA_CONTENT:
                view.setContent(mCinemas);
                view.showContent();
                break;
            case SHOW_CINEMA_LOADING:
                view.showLoading();
                break;
            case SHOW_CINEMA_ERROR:
                view.showError();
                break;
            default:
                view.showError();
                break;
        }
    }
}
