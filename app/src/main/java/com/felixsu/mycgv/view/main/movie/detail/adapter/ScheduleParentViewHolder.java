package com.felixsu.mycgv.view.main.movie.detail.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;

import java.util.ArrayList;

/**
 * Created on 12/21/16.
 *
 * @author felixsoewito
 */

public class ScheduleParentViewHolder extends ActiveListViewHolder<ArrayList<Schedule>> {

    private TextView mNameLabel;
    private RecyclerView mListContainer;

    private final Context mContext;

    public ScheduleParentViewHolder(View itemView,
                                    ListAdapter.ItemClickListener itemClickListener,
                                    ListAdapter.ItemLongClickListener itemLongClickListener,
                                    Context context) {
        super(itemView, itemClickListener, itemLongClickListener);
        mContext = context;

        mNameLabel = (TextView) itemView.findViewById(R.id.label_name);
        mListContainer = (RecyclerView) itemView.findViewById(R.id.container_schedule_child);
        mListContainer.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mListContainer.setHasFixedSize(true);
        mListContainer.setNestedScrollingEnabled(false);
    }

    @Override
    public void bindView(ArrayList<Schedule> item) {
        String name = item.get(0).getCinemaName();
        ScheduleFirstChildAdapter adapter = new ScheduleFirstChildAdapter(mContext, item);

        mNameLabel.setText(name);
        mListContainer.setAdapter(adapter);
    }
}
