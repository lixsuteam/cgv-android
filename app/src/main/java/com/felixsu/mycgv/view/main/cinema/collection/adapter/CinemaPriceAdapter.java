package com.felixsu.mycgv.view.main.cinema.collection.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;

import java.util.List;

/**
 * Created on 12/23/16.
 *
 * @author felixsoewito
 */

public class CinemaPriceAdapter extends ListAdapter<CinemaPrice, CinemaPriceViewHolder> {

    public CinemaPriceAdapter(@NonNull List<CinemaPrice> items) {
        super(items);
    }

    @Override
    public CinemaPriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_cinema_price, parent, false);
        return new CinemaPriceViewHolder(view, mItemClickListener, mItemLongClickListener);
    }
}
