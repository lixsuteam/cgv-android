package com.felixsu.mycgv.view.main.cinema.collection;

import android.util.Log;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 12/19/16.
 *
 * @author felixsoewito
 */
public class CinemaCollectionPresenter extends MvpBasePresenter<CinemaCollectionView> {

    private static final String TAG = CinemaCollectionPresenter.class.getName();

    private Subscriber<RestResponse<Cinema>> mCinemaSubscriber;
    private CgvService mService;
    private SharedData mSharedData;

    @Inject
    public CinemaCollectionPresenter(CgvService service, SharedData sharedData) {
        mService = service;
        mSharedData = sharedData;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            cancelSubscription();
        }
    }

    public void load() {
        subscribeCinema(mService.getCinemas(mSharedData.getCity(), Cinema.CGV));
    }

    private void subscribeCinema(Observable<RestResponse<Cinema>> observable) {

        if (mSharedData.getCinemaContent() != null) {
            if (isViewAttached()) {
                getView().setContent(mSharedData.getCinemaContent());
                getView().showContent();
            }
            return;
        }

        if (isViewAttached()) {
            getView().showLoading();
        }

        cancelSubscription();

        mCinemaSubscriber = new Subscriber<RestResponse<Cinema>>() {
            @Override
            public void onCompleted() {
                if (isViewAttached()) {
                    getView().showContent();
                }
                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                if (isViewAttached()) {
                    getView().showError();
                }
                unsubscribe();
            }

            @Override
            public void onNext(RestResponse<Cinema> response) {
                List<Cinema> cinemas = response.getResponses();

                mSharedData.putCinemaContent(cinemas);

                if (isViewAttached()) {
                    getView().setContent(cinemas);
                }
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mCinemaSubscriber);
    }

    private void cancelSubscription() {
        if (mCinemaSubscriber != null && !mCinemaSubscriber.isUnsubscribed()) {
            mCinemaSubscriber.unsubscribe();
        }
        mCinemaSubscriber = null;
    }
}
