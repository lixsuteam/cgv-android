package com.felixsu.mycgv.view.main.movie.collection.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.BuildConfig;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public class ComingSoonViewHolder extends ActiveListViewHolder<Movie> {

    private ImageView mCoverImage;

    private final Context mContext;

    public ComingSoonViewHolder(View itemView,
                                ListAdapter.ItemClickListener itemClickListener,
                                ListAdapter.ItemLongClickListener itemLongClickListener,
                                Context context) {
        super(itemView, itemClickListener, itemLongClickListener);
        mContext = context;

        mCoverImage = (ImageView) itemView.findViewById(R.id.image_cover);
    }

    @Override
    public void bindView(Movie movie) {
        Picasso.with(mContext).load(BuildConfig.CGV_BASE_URL + movie.getCoverUrl())
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(mCoverImage);
    }
}
