package com.felixsu.mycgv.view.main.cinema.collection.adapter;

import android.view.View;
import android.widget.TextView;

import com.felixsu.common.helper.PriceTypeMapper;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.felixsu.mycgv.helper.Util;

/**
 * Created by felixsoewito on 12/22/16.
 */

public class CinemaPriceViewHolder extends ActiveListViewHolder<CinemaPrice> {

    private TextView mScheduleClassLabel;
    private TextView mPriceRegular;
    private TextView mPriceOcassional;
    private TextView mPriceHoliday;

    public CinemaPriceViewHolder(View itemView,
                                 ListAdapter.ItemClickListener itemClickListener,
                                 ListAdapter.ItemLongClickListener itemLongClickListener) {
        super(itemView, itemClickListener, itemLongClickListener);

        mScheduleClassLabel = (TextView) itemView.findViewById(R.id.label_schedule_class);
        mPriceRegular = (TextView) itemView.findViewById(R.id.label_price_regular);
        mPriceOcassional = (TextView) itemView.findViewById(R.id.label_price_middle);
        mPriceHoliday = (TextView) itemView.findViewById(R.id.label_price_holiday);
    }

    @Override
    public void bindView(CinemaPrice item) {
        mScheduleClassLabel.setText(PriceTypeMapper.getName(item.getType()));
        mPriceRegular.setText(formatPrice(item.getNormalPrice()));
        mPriceOcassional.setText(formatPrice(item.getOccasionalPrice()));
        mPriceHoliday.setText(formatPrice(item.getHolidayPrice()));
    }

    private String formatPrice(String price) {
        Long l = Long.valueOf(price);
        String prettyPrice = Util.formatAsNumber(l);
        return "IDR " + prettyPrice;
    }
}
