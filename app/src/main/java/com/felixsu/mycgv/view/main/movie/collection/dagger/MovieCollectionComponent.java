package com.felixsu.mycgv.view.main.movie.collection.dagger;

import com.felixsu.mycgv.common.dagger.PerFragment;
import com.felixsu.mycgv.common.dagger.component.CgvAppComponent;
import com.felixsu.mycgv.view.main.movie.collection.MovieCollectionFragment;
import com.felixsu.mycgv.view.main.movie.collection.MovieCollectionPresenter;

import dagger.Component;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

@PerFragment
@Component(
        dependencies = CgvAppComponent.class
)
public interface MovieCollectionComponent {

    MovieCollectionPresenter presenter();

    void inject(MovieCollectionFragment fragment);

}
