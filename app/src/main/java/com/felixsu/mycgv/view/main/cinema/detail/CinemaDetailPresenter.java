package com.felixsu.mycgv.view.main.cinema.detail;

import android.util.Log;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.provider.data.local.SharedData;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

public class CinemaDetailPresenter extends MvpBasePresenter<CinemaDetailView> {

    private static final String TAG = CinemaDetailPresenter.class.getName();

    public static final long SCHEDULE_VALIDITY = 10*60*1000; //10 minutes

    private Subscriber<RestResponse<Schedule>> mCinemaSubscriber;
    private SharedData mSharedData;
    private CgvService mCgvService;
    private String mCinemaId;

    @Inject
    public CinemaDetailPresenter(SharedData sharedData, CgvService cgvService) {
        mSharedData = sharedData;
        mCgvService = cgvService;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            cancelSubscription();
        }
    }

    public void load(String cinemaId) {
        mCinemaId = cinemaId;
        subscribeSchedule(mCgvService.getSchedules(cinemaId));
    }

    private void subscribeSchedule(Observable<RestResponse<Schedule>> observable){
        long currentTime = new Date().getTime();
        long lastStore = mSharedData.getCinemaScheduleSynchronized() == null ? 0L : mSharedData.getCinemaScheduleSynchronized();
        String fetchedLastTime = mSharedData.getCinemaScheduleFetched() == null ? "" : mSharedData.getCinemaScheduleFetched();

        boolean isScheduleValid = (currentTime - lastStore) < SCHEDULE_VALIDITY;
        boolean isLoaded = mSharedData.getCinemaScheduleContent() != null;
        boolean isScheduleEquals = fetchedLastTime.equals(mCinemaId);

        if (isLoaded && isScheduleValid && isScheduleEquals) {
            if (isViewAttached()) {
                getView().setContent(mSharedData.getCinemaScheduleContent());
                getView().showContent();
            }
            return;
        }

        if (isViewAttached()) {
            getView().showLoading();
        }

        cancelSubscription();

        mCinemaSubscriber = new Subscriber<RestResponse<Schedule>>() {
            @Override
            public void onCompleted() {
                if (isViewAttached()) {
                    getView().showContent();
                }
                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                if (isViewAttached()) {
                    getView().showError();
                }
                unsubscribe();
            }

            @Override
            public void onNext(RestResponse<Schedule> response) {
                List<Schedule> schedules = response.getResponses();
                Map<String, ArrayList<Schedule>> mapper = new HashMap<>();

                for (Schedule s : schedules) {
                    String movie = s.getMovieId();

                    if (mapper.containsKey(movie)) {
                        mapper.get(movie).add(s);
                    } else {
                        ArrayList<Schedule> l = new ArrayList<>();
                        l.add(s);
                        mapper.put(movie, l);
                    }
                }

                ArrayList<ArrayList<Schedule>> result = new ArrayList<>();
                for (Map.Entry<String, ArrayList<Schedule>> entry : mapper.entrySet()) {
                    result.add(entry.getValue());
                }

                mSharedData.putCinemaScheduleSynchronized(new Date().getTime());
                mSharedData.putCinemaScheduleContent(result);
                mSharedData.putCinemaScheduleFetched(mCinemaId);

                if (isViewAttached()) {
                    getView().setContent(result);
                }
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mCinemaSubscriber);
    }

    private void cancelSubscription() {
        if (mCinemaSubscriber != null && !mCinemaSubscriber.isUnsubscribed()) {
            mCinemaSubscriber.unsubscribe();
        }
        mCinemaSubscriber = null;
    }


}
