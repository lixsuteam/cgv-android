package com.felixsu.mycgv.view.main.cinema.detail.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.provider.data.remote.CgvService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by felixsoewito on 12/24/16.
 */

public class ScheduleParentAdapter extends ListAdapter<ArrayList<Schedule>, ScheduleParentViewHolder> {

    private final CgvService mCgvService;
    private final Context mContext;

    public ScheduleParentAdapter(Context context, CgvService cgvService, @NonNull List<ArrayList<Schedule>> items) {
        super(items);
        mContext = context;
        mCgvService = cgvService;
    }

    @Override
    public ScheduleParentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_schedule_parent_cinema, parent, false);
        return new ScheduleParentViewHolder(view, mItemClickListener, mItemLongClickListener, mContext, mCgvService);
    }

    @Override
    public void onBindViewHolder(ScheduleParentViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.load();
    }

    @Override
    public void onViewRecycled(ScheduleParentViewHolder holder) {
        holder.unsubscribe();
        super.onViewRecycled(holder);
    }
}
