package com.felixsu.mycgv.view.main.movie.collection.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Movie;
import com.felixsu.mycgv.BuildConfig;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created on 12/18/16.
 *
 * @author felixsoewito
 */

public class NowPLayingViewHolder extends ActiveListViewHolder<Movie> {

    private TextView mTitleLabel;
    private ImageView mCoverPicture;
    private TextView mDurationLabel;
    private TextView mRatingLabel;
    private TextView mGenreLabel;

    final private Context mContext;

    public NowPLayingViewHolder(View itemView,
                                ListAdapter.ItemClickListener itemClickListener,
                                ListAdapter.ItemLongClickListener itemLongClickListener,
                                Context context) {
        super(itemView, itemClickListener, itemLongClickListener);
        mContext = context;

        mTitleLabel = (TextView) itemView.findViewById(R.id.label_title);
        mDurationLabel = (TextView) itemView.findViewById(R.id.label_duration);
        mRatingLabel = (TextView) itemView.findViewById(R.id.label_rating);
        mGenreLabel = (TextView) itemView.findViewById(R.id.label_genre);

        mCoverPicture = (ImageView) itemView.findViewById(R.id.image_cover);
    }

    @Override
    public void bindView(Movie movie) {
        Picasso.with(mContext).load(BuildConfig.CGV_BASE_URL + movie.getCoverUrl())
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(mCoverPicture);

        mTitleLabel.setText(movie.getMovieName());
        mDurationLabel.setText(String.valueOf(movie.getDuration()));
        mRatingLabel.setText(movie.getRating());
        mGenreLabel.setText(movie.getGenre());
    }
}
