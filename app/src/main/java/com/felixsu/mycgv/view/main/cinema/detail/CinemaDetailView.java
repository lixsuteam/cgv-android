package com.felixsu.mycgv.view.main.cinema.detail;

import com.felixsu.common.model.cinema.Schedule;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.ArrayList;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

public interface CinemaDetailView extends MvpView {

    void showContent();
    void showError();
    void showLoading();

    void setContent(ArrayList<ArrayList<Schedule>> schedules);
    void setError(String message);

    ArrayList<ArrayList<Schedule>> getContent();
    String getError();
}
