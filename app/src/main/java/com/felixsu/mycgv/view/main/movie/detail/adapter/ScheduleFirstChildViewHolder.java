package com.felixsu.mycgv.view.main.movie.detail.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.common.view.adapter.ActiveListViewHolder;
import com.felixsu.mycgv.common.view.adapter.ListViewHolder;
import com.felixsu.mycgv.helper.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/21/16.
 *
 * @author by felixsoewito
 */

public class ScheduleFirstChildViewHolder extends ListViewHolder<Schedule> {

    private final Context mContext;
    private TextView mDetailLabel;
    private RecyclerView mScheduleContentContainer;

    public ScheduleFirstChildViewHolder(View itemView, Context context) {
        super(itemView);
        mContext = context;

        mDetailLabel = (TextView) itemView.findViewById(R.id.label_detail);
        mScheduleContentContainer = (RecyclerView) itemView.findViewById(R.id.container_schedule_second_child);
        mScheduleContentContainer.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mScheduleContentContainer.setHasFixedSize(true);
        mScheduleContentContainer.setNestedScrollingEnabled(false);
    }

    @Override
    public void bindView(Schedule item) {
        String scheduleClass = item.getScheduleClass();
        String detail = String.format("Class %s", scheduleClass);

        final String token = "#";
        List<String> result = new ArrayList<>();
        String[] playingTimes = item.getPlayingTime().split(token);

        int i = 0;
        String element = "";
        for (String s : playingTimes) {
            element = element.concat(s);
            if (i == 3) {
                i = 0;
                result.add(element);
                element = "";
            } else {
                element = element.concat(token);
                i++;
            }
        }

        if (!element.isEmpty()) {
            result.add(element);
        }

        ScheduleSecondChildAdapter adapter = new ScheduleSecondChildAdapter(mContext, item, result);

        mDetailLabel.setText(detail);
        mScheduleContentContainer.setAdapter(adapter);

    }
}
