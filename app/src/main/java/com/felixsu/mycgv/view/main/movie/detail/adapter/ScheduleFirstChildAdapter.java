package com.felixsu.mycgv.view.main.movie.detail.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;

import java.util.List;

/**
 * Created on 12/21/16.
 *
 * @author felixsoewito
 */

public class ScheduleFirstChildAdapter extends ListAdapter<Schedule, ScheduleFirstChildViewHolder> {

    private final Context mContext;

    public ScheduleFirstChildAdapter(Context context, @NonNull List<Schedule> items) {
        super(items);
        mContext = context;
    }

    @Override
    public ScheduleFirstChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_schedule_first_child, parent, false);
        return new ScheduleFirstChildViewHolder(view, mContext);
    }
}
