package com.felixsu.mycgv.view.main.cinema.collection.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.adapter.ListAdapter;
import com.felixsu.mycgv.helper.CinemaHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created on 12/19/16.
 *
 * @author felixsoewito
 */

public class CinemaAdapter extends PagerAdapter {

    private List<Cinema> mCinemas;
    private final LayoutInflater mLayoutInflater;
    private final Context mContext;
    private final CinemaClickListener mClickListener;

    public CinemaAdapter(final List<Cinema> cinemas, final Context context, CinemaClickListener clickListener) {
        mCinemas = cinemas;
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
        mClickListener = clickListener;
    }

    @Override
    public int getCount() {
        return mCinemas.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = mLayoutInflater.inflate(R.layout.vh_cinema, container, false);

        ImageView cinemaCoverImage = (ImageView) view.findViewById(R.id.image_cover);
        TextView cinemaNameLabel = (TextView) view.findViewById(R.id.label_cinema_name);

        View cinemaNoDetailContainer = view.findViewById(R.id.container_cinema_no_detail);
        View cinemaDetailContainer = view.findViewById(R.id.container_cinema_detail);
        View cinemaNoPriceContainer = view.findViewById(R.id.container_cinema_no_price);
        View cinemaPriceContainer = view.findViewById(R.id.container_cinema_price);

        final Cinema cinema = mCinemas.get(position);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(v, cinema);
                }
            }
        });

        Picasso.with(mContext).load(cinema.getCover())
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(cinemaCoverImage);
        cinemaNameLabel.setText(cinema.getCinemaName());

        //manage cinema details view
        if (cinema.getCinemaDetails() != null && !cinema.getCinemaDetails().isEmpty() ){
            cinemaNoDetailContainer.setVisibility(View.GONE);
            cinemaDetailContainer.setVisibility(View.VISIBLE);
            showCinemaDetails(view, cinema.getCinemaDetails());
        } else {
            cinemaNoDetailContainer.setVisibility(View.VISIBLE);
            cinemaDetailContainer.setVisibility(View.GONE);
        }

        //manage cinema prices view
        if (cinema.getCinemaPrices() != null && !cinema.getCinemaPrices().isEmpty()){
            cinemaNoPriceContainer.setVisibility(View.GONE);
            cinemaPriceContainer.setVisibility(View.VISIBLE);
            showCinemaPrices(view, cinema.getCinemaPrices());
        } else {
            cinemaNoPriceContainer.setVisibility(View.VISIBLE);
            cinemaPriceContainer.setVisibility(View.GONE);
        }

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setCinemas(final List<Cinema> cinemas){
        mCinemas = cinemas;
        notifyDataSetChanged();
    }

    public List<Cinema> getCinemas(){
        return mCinemas;
    }

    private void showCinemaDetails(View rootView, List<CinemaDetail> details) {
        TextView primaryAddressLabel = (TextView) rootView.findViewById(R.id.label_cinema_address);
        TextView secondaryAddressLabel = (TextView) rootView.findViewById(R.id.label_cinema_address_2);

        String[] addresses = CinemaHelper.findAddress(details);

        if (addresses.length > 0) {
            primaryAddressLabel.setText(addresses[0]);
        }
        if (addresses.length > 1) {
            secondaryAddressLabel.setText(addresses[1]);
        }
    }

    private void showCinemaPrices(View rootView, List<CinemaPrice> prices) {
        CinemaPriceAdapter adapter = new CinemaPriceAdapter(prices);

        RecyclerView mPriceContentContainer = (RecyclerView) rootView.findViewById(R.id.container_cinema_price_content);
        mPriceContentContainer.setLayoutManager(new GridLayoutManager(mContext, 2, LinearLayoutManager.VERTICAL, false));
        mPriceContentContainer.setHasFixedSize(true);
        mPriceContentContainer.setAdapter(adapter);
    }

    public interface CinemaClickListener {

        void onItemClick(View v, Cinema cinema);
    }
}
