package com.felixsu.mycgv.view.main.movie.passive;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.felixsu.mycgv.BuildConfig;
import com.felixsu.mycgv.R;
import com.felixsu.mycgv.common.view.fragment.BaseFragment;
import com.felixsu.mycgv.helper.FirebaseHelper;
import com.felixsu.mycgv.helper.MovieHelper;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import icepick.State;

/**
 * Created on 08/01/2017.
 *
 * @author felixsu
 */
public class MovieDetailPassiveFragment extends BaseFragment {

    public static final String TAG = MovieDetailPassiveFragment.class.getName();

    @BindView(R.id.container_primary)
    View mPrimaryContainer;
    @BindView(R.id.label_title)
    TextView mTitleLabel;
    @BindView(R.id.image_cover)
    ImageView mCoverImage;
    @BindView(R.id.label_duration) TextView mDurationLabel;
    @BindView(R.id.label_rating) TextView mRatingLabel;
    @BindView(R.id.label_directors) TextView mDirectorsLabel;
    @BindView(R.id.label_genre) TextView mGenreLabel;
    @BindView(R.id.label_language) TextView mLanguageLabel;
    @BindView(R.id.label_movie_actors) TextView mActorsLabel;
    @BindView(R.id.label_description_value) TextView mDescriptionLabel;

    //DATA
    @State @Arg String mId;
    @State @Arg String mTitle;
    @State @Arg String mCoverUrl;
    @State @Arg String mDuration;
    @State @Arg String mRating;
    @State @Arg String mDirectors;
    @State @Arg String mGenre;
    @State @Arg String mLanguage;
    @State @Arg String mActors;
    @State @Arg String mDescription;
    @State @Arg String mTrailerUrl;

    //LISTENER
    private View.OnClickListener mMovieClickListener;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMovieClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTrailerUrl != null) {
                    MovieHelper.openTrailer(getContext(), mTrailerUrl);
                    Toast.makeText(getContext(), R.string.text_opening_trailer, Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseHelper.logEventMovieError(FirebaseAnalytics.getInstance(getContext()), mId);
                    Toast.makeText(getContext(), R.string.text_trailer_not_available, Toast.LENGTH_SHORT).show();
                }
            }
        };
        mPrimaryContainer.setOnClickListener(mMovieClickListener);
        mTitleLabel.setText(mTitle);
        mDurationLabel.setText(mDuration);
        mRatingLabel.setText(mRating);
        mDirectorsLabel.setText(mDirectors);
        mGenreLabel.setText(mGenre);
        mLanguageLabel.setText(mLanguage);
        mActorsLabel.setText(mActors);
        mDescriptionLabel.setText(mDescription);
        Picasso.with(getContext()).load(BuildConfig.CGV_BASE_URL + mCoverUrl)
                .placeholder(R.drawable.cgv_empty)
                .error(R.drawable.cgv_empty)
                .into(mCoverImage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMovieClickListener = null;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_movie_detail_passive;
    }
}
