package com.felixsu.mycgv.view.main.cinema.detail.dagger;

import com.felixsu.mycgv.common.dagger.PerFragment;
import com.felixsu.mycgv.common.dagger.component.CgvAppComponent;
import com.felixsu.mycgv.provider.data.remote.CgvService;
import com.felixsu.mycgv.view.main.cinema.detail.CinemaDetailFragment;
import com.felixsu.mycgv.view.main.cinema.detail.CinemaDetailPresenter;

import dagger.Component;

/**
 * Created on 12/24/16.
 *
 * @author felixsoewito
 */

@PerFragment
@Component(
        dependencies = CgvAppComponent.class
)
public interface CinemaDetailComponent {

    CinemaDetailPresenter presenter();

    CgvService cgvService();

    void inject(CinemaDetailFragment fragment);
}
