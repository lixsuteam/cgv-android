package com.felixsu.mycgv.model.movie;

import com.felixsu.common.model.cinema.Movie;

import java.util.List;
import java.util.Map;

/**
 * Created on 12/16/16.
 *
 * @author felixsoewito
 */


public interface MovieManager {

    List<Movie> findAll(Map<String,String> param);

}
