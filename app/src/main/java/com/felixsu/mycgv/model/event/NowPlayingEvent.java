package com.felixsu.mycgv.model.event;

import java.util.List;
import com.felixsu.common.model.cinema.Movie;


/**
 * Created on 12/15/16.
 *
 * @author felixsoewito
 */


public class NowPlayingEvent {

    private List<Movie> mMovies;

    public NowPlayingEvent(List<Movie> movies) {
        mMovies = movies;
    }

    public List<Movie> getMovies() {
        return mMovies;
    }
}
