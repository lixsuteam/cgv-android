package com.felixsu.mycgv.model.event;

import com.felixsu.common.model.cinema.Schedule;

import java.util.List;

/**
 * Created on 12/15/16.
 *
 * @author felixsoewito
 */


public class ScheduleEvent {

    private List<Schedule> mSchedules;

    public ScheduleEvent(List<Schedule> schedules) {
        mSchedules = schedules;
    }

    public List<Schedule> getSchedules() {
        return mSchedules;
    }
}
