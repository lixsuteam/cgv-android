package com.felixsu.mycgv;

import android.app.Application;
import android.util.Log;

import com.felixsu.mycgv.common.dagger.component.CgvAppComponent;
import com.felixsu.mycgv.common.dagger.component.DaggerCgvAppComponent;
import com.felixsu.mycgv.common.dagger.module.AppModule;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import okhttp3.OkHttpClient;

/**
 * Base App for MyCGV App
 * Created on 12/17/16.
 *
 * @author felixsoewito
 */

public class MyCgvApp extends Application {

    private static final String TAG = MyCgvApp.class.getName();

    CgvAppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerCgvAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(new OkHttpClient())).build();
        try {
            Picasso.setSingletonInstance(picasso);
        } catch (IllegalStateException ignored) {
            Log.e(TAG, "failed instantiate picasso");
        }
    }

    public CgvAppComponent getComponent() {
        return mComponent;
    }
}
