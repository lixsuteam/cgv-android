package com.felixsu.test.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by felixsoewito on 11/24/16.
 */
public class JsonHelper {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static ObjectMapper getObjectMapperInstance(){
        return OBJECT_MAPPER;
    }

    public static String stringify(Object o) throws IOException {
        return OBJECT_MAPPER.writeValueAsString(o);
    }

    public static <T> T fromJson(String json, Class<T> clazz) throws IOException {
        return OBJECT_MAPPER.readValue(json, clazz);
    }
}
