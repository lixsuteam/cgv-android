package com.felixsu.test.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by felixsoewito on 9/6/16.
 */
public class DateHelper {
    private static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final SimpleDateFormat ISO_8601_DATE_FORMATTER = new SimpleDateFormat(ISO_8601_FORMAT);

    public static SimpleDateFormat getIso8601DateFormatterInstance(){
        return ISO_8601_DATE_FORMATTER;
    }

    public static String getIso8601DateFormatterString(Date d){
        return ISO_8601_DATE_FORMATTER.format(d);
    }

    public static String getIso8601DateFormatterString(){
        Date d = new Date();
        return getIso8601DateFormatterString(d);
    }

    public static long getTime(){
        return new Date().getTime();
    }
}
