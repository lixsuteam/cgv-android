package com.felixsu.test.model.general;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by felixsoewito on 11/13/16.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Error {

    private String code;
    private String message;
    private String stackTrace;

    public static Error fromThrowable(String code, Throwable throwable) {
        return new Error(code, throwable.getMessage());
    }

    public static Error fromMessage(String code, String message) {
        return new Error(code, message);
    }

    public Error() {
    }

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public Error(String code, String message, String stackTrace) {
        this(code, message);
        this.stackTrace = stackTrace;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
