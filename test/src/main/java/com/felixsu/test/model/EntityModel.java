package com.felixsu.test.model;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/13/16.
 */
public interface EntityModel<ID extends Serializable> extends CommonModel {
    ID getId();
}
