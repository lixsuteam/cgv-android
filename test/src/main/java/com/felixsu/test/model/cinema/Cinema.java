package com.felixsu.test.model.cinema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.felixsu.test.model.EntityModel;
import com.felixsu.test.model.extra.CgvItem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by felixsoewito on 11/9/16.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cinema implements
        EntityModel<String>,
        Serializable,
        CgvItem {

    private String id;
    private String cinemaName;
    private String city;
    private String cover;
    private List<CinemaDetail> cinemaDetails;
    private List<CinemaPrice> cinemaPrices;

    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;

    public Cinema() {
    }

    public Cinema(String id) {
        this.id = id;
    }

    public Cinema(String id, String cinemaName, String city) {
        this(id);
        this.cinemaName = cinemaName;
        this.city = city;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<CinemaDetail> getCinemaDetails() {
        return cinemaDetails;
    }

    public void setCinemaDetails(List<CinemaDetail> cinemaDetails) {
        this.cinemaDetails = cinemaDetails;
    }

    public List<CinemaPrice> getCinemaPrices() {
        return cinemaPrices;
    }

    public void setCinemaPrices(List<CinemaPrice> cinemaPrices) {
        this.cinemaPrices = cinemaPrices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cinema cinema = (Cinema) o;

        return id != null ? id.equals(cinema.id) : cinema.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
