package com.felixsu.test.model.general;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.felixsu.test.model.CommonModel;

/**
 * Created by felixsoewito on 12/2/16.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Acknowledge implements CommonModel {

    private String message;

    public Acknowledge() {
    }

    public Acknowledge(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
