package com.felixsu.test.exception;

/**
 * Created by felixsoewito on 12/2/16.
 */
public class FsMultipleChoicesException extends Exception {

    public FsMultipleChoicesException(String message) {
        super(message);
    }

    public FsMultipleChoicesException(String message, Throwable cause) {
        super(message, cause);
    }

    public FsMultipleChoicesException(Throwable cause) {
        super(cause);
    }
}
