package com.felixsu.test.exception;

/**
 * Created by felixsoewito on 11/14/16.
 */
public class FSNotFoundException extends Exception {

    public FSNotFoundException(String message) {
        super(message);
    }

    public FSNotFoundException(Throwable cause) {
        super(cause);
    }

    public FSNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
